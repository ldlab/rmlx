<?php

require_once("ext/jsonpath-0.8.1.php");
header('Content-type: text/plain');
$json = json_decode(file_get_contents($_GET['src']), true);
echo json_encode(jsonPath($json, $_GET['q']));
?>