<?php
/*
 * RML Processor controller logic for [botol](https://github.com/pebbie/botol)
 * dependency : 
 * + [EasyRdf](https://github.com/njh/easyrdf)
 * + jsonpath
 * + [Spreadsheet-Reader](https://github.com/nuovo/spreadsheet-reader)

 * Notes :
 * for spreadsheet there is a small fix, see my comment in https://github.com/nuovo/spreadsheet-reader/issues/69

 * TODO:
 * - add rmlx:predicatePath for generating complex object values (e.g. geo:location/geo:longitude) without TripleMaps joins
 * - rop prefix separator should be using / instead of # so that it can be interpreted as http service
 * - interpret unknown URI referenced using rml:referenceFormulation as online services (in hydra)

 * 20151204 :
 * - apply selector on remote service response (extract value from json)
 * - force prefix namespace 

 * 20151202 :
 * - iterate pivot table from excel spreadsheet by unpivoting

 * 20151109 :
 * - add rmlx:predicateInv for creating an inverse property relationship
 * - implement rr:BlankNode as rr:termType value in subjectMap/objectMap 
 * - add caching for http transform functions
 * - enable ignore empty string on object value
 * - add retry on making http request if timeout
 * - introduce conditional auto_increment variable (based on value occurence of some column/reference)
 */

function _exp($s)
{
    return EasyRdf_Namespace::expand($s);
}

function get_api($location, $src_path = "tmp/")
{
    global $api_cache;
    if(!array_key_exists($location, $api_cache))
    {
        return null;
    }
    return $api_cache[$location];
}

function time_elapsed()
{
    static $last = null;

    $now = microtime(true);

    if ($last != null) {
        //echo "\n".($now - $last);
    }

    $last = $now;
}

function cfile_get_contents($uri)
{
    $pf = "";
    $method = "GET";
    $cr = new CurlResponse();
    $ch = curl_init();
    $options = array(
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST => $method,
        CURLOPT_URL => $uri.(($method=="POST")?'':$pf),
        CURLOPT_TIMEOUT => 30,
        CURLOPT_POST => $method=="POST",
        CURLOPT_POSTFIELDS => $pf
        );
    #print_r($options);
    curl_setopt_array($ch, $options);
    $cr->register($ch);

    $output = curl_exec($ch);
    return $output;
}

function get_content($location, $default_ext, $src_path = "tmp/")
{
    global $content_cache, $global_vars;
    if(!array_key_exists($location, $content_cache))
    {
        if($location)
        $content = file_get_contents($location);
        $content_cache[$location] = $content;
        $content_file = $src_path.md5($location).'-'.md5($content).$default_ext;
        file_put_contents($content_file, $content);
    }
    return $content_cache[$location];
}

function get_contentfile($location, $default_ext, $src_path = "tmp/")
{
    global $content_cache, $global_vars;
    if(!array_key_exists($location, $content_cache))
    {
        if($location)
        $content = file_get_contents($location);
        $content_cache[$location] = $content;
        $content_file = $src_path.md5($location).'-'.md5($content).$default_ext;
        file_put_contents($content_file, $content);
    }
    return $content_file;
}

function make_request($uri, $params, $method="GET", $retries=3, $sleep_us=0)
{
    global $request_cache;
    
    $retry = 0;
    
    $pf = http_build_query($params, '', '&');
    #var_dump($uri.$pf);
    $reqmd5 = md5($uri.$pf);
    $req_cache_file = "tmp/".$reqmd5.".httpcache";

    if(array_key_exists($reqmd5, $request_cache))
    {
        return $request_cache[$reqmd5];
    }
    else if(file_exists($req_cache_file))
    {
        $request_cache[$reqmd5] = file_get_contents($req_cache_file);
        return $request_cache[$reqmd5];
    }

    $cr = new CurlResponse();
    $ch = curl_init();
    $options = array(
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST => $method,
        CURLOPT_URL => $uri.(($method=="POST")?'':$pf),
        CURLOPT_TIMEOUT => 30,
        CURLOPT_POST => $method=="POST",
        CURLOPT_POSTFIELDS => $pf
        );
    #print_r($options);
    curl_setopt_array($ch, $options);
    $cr->register($ch);
    
    do{ 
        if($retry>0) usleep($sleep_us);
        $output = curl_exec($ch);    
    }
    while((!($cr->headers["HTTP-Code"]=="200") && $retry<$retries) || curl_errno($ch)==28);//timeout = 28
    
    if($cr->headers["HTTP-Code"])
    {
        //cache result
        file_put_contents($req_cache_file, $output);
        $request_cache[$reqmd5] = $output;
    }
    return $output;
}

function _depth($node, $dep)
{
    if(!array_key_exists($node, $dep)) return 0;
    $branch = array();
    foreach($dep[$node] as $key => $value){
        $branch[] = _depth($value[1], $dep);
    }
    return 1+max($branch);
}

function _vdepth($vname, $vdep)
{
    if(!array_key_exists($vname, $vdep)) return 0;
    $branch = 0;
    foreach($vdep[$vname] as $key => $value){
        $branch += _vdepth($value, $vdep);
    }
    return 1+$branch;
}

$request_cache = array();
$content_cache = array();
$api_cache = array();
$global_vars = array();

function extract_vars($template)
{
    $out = array();
    $tmp = explode("{", $template);
    unset($tmp[0]);
    foreach($tmp as $key => $value)
    {
        $tmp[$key] = substr($value, 0, strpos($value,"}"));
    }
    return $tmp;
}

class RmlSource
{
    public function open($location)
    {
        throw new EasyRdf_Exception(
            "This method should be overridden by sub-classes."
        );
    }

    public function iterate($iterator, $ref=null)
    {
        throw new EasyRdf_Exception(
            "This method should be overridden by sub-classes."
        );
    }

    public function lookup($reference, $ref=null)
    {
        throw new EasyRdf_Exception(
            "This method should be overridden by sub-classes."
        );
    }
}

class PDOSource extends RmlSource
{
    private $db = NULL;

    public function open($location)
    {
        try
        {
            $this->db = new PDO($location);
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function iterate($iterator, $ref=null)
    {
        $stmt = $this->db->prepare($iterator);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function lookup($reference, $ref=null)
    {
        if(!$ref) return "";
        if(array_key_exists($reference, $ref))
        {
            return $ref[$reference];
        }
    }
}

class CSSSource extends RmlSource
{
    private $css;
    public function open($location)
    {
        $this->css = new SelectorDom(get_content($location, ".html"));
    }

    public function iterate($iterator, $ref=null)
    {
        $sel = $this->css->select($iterator);
        #print_r($sel);
        return $sel;
    }

    public function lookup($reference, $ref=null)
    {
        #print_r($ref);
        if($ref)
            if(is_array($ref) && array_key_exists($reference, $ref))
            {
                #print_r($ref[$reference]);
                return $ref[$reference];
            }
        else 
            return $this->css->select($reference);          
    }
}

class RegExpSource extends RmlSource
{
    private $content = "";

    public function open($location)
    {
        $this->content = get_content($location, ".rc");
    }

    public function iterate($iterator, $ref=null)
    {
        $matches = preg_split($iterator, $this->content, -1, PREG_SPLIT_OFFSET_CAPTURE);
        $rows = array();
        foreach($matches as $key => $value)
        {
            $row = array();
            $row['@text'] = $value[0];
            $row['@pos'] = $value[1];
            $rows[] = $row;
        }
        return $rows;
    }

    public function lookup($reference, $ref=null)
    {
        if(array_key_exists($reference, $ref))
        {
            return $ref[$reference];
        }
        else
        {
            $matches = array();
            preg_match_all($reference, $ref["@text"], $matches);
            return $matches[1];
        }
    }
}

class JsonSource extends RmlSource
{
    private $parser;
    private $json;

    public function __construct()
    {
        if($this->parser==null)
            $this->parser = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
    }

    public function open($location)
    {
        //$this->json = $this->parser->decode(get_content($location, ".json"));
        $this->json = json_decode(get_content($location, ".json"), true);

    }

    public function iterate($iterator, $ref=null)
    {
        //echo $iterator;
        //print_r($iterator);
        $tmp = jsonPath($this->json, $iterator);
        //print_r($tmp);
        if($tmp===false)
            return array($this->json);
        if ($ref==null)
            return jsonPath($this->json, $iterator);
        else
            return jsonPath($ref, $iterator);
    }

    public function lookup($reference, $ref=null)
    {
        //print_r($reference);
        if ($ref==null || $ref===false)
            return jsonPath($this->json, $reference);
        else
            return jsonPath($ref, $reference);
    }
}

class CsvSource extends RmlSource
{
    private $csv;
    private $header;
    private $delimiter = ',';
    private $rowid;
    private $location;

    public function __construct()
    {
        
    }

    public function __destruct()
    {
        if($this->csv)
            fclose($this->csv);
    }

    public function open($location, $ref=null)
    {
        $this->location = $location;
        /*
        global $content_cache;
        if(!array_key_exists($location, $content_cache))
        {
            $src_path = "tmp/";
            $content = file_get_contents($location);
            $csv_file = $src_path.md5($location).'-'.md5($content).".csv";
            file_put_contents($csv_file, $content);
            $content_cache[$location] = $csv_file;
        }
        */
        //$this->csv = fopen($content_cache[$location], 'r');
        $fp = fopen("php://memory", "r+");
        fputs($fp, get_content($location, ".csv"));
        rewind($fp);       
        $this->csv = $fp;        
        $this->rowid = 0;
    }

    public function iterate($iterator, $ref=null)
    {
        switch($iterator)
        {
            case ',':
            case ';':
            case '|':
            case '\t':
                $this->delimiter = $iterator;
                break;
        }
        
        $this->header = fgetcsv($this->csv, 1000, $this->delimiter);
        foreach($this->header as $idx => $head)
        {
            $this->header[$idx] = str_replace(" ", "_", $head);
        }
        //print_r($this->header);
        $tmp = array();
        $this->rowid = 0;
        while(($row = fgetcsv($this->csv, 1000, $this->delimiter)) != FALSE)
        {
            $ch = count($this->header);
            $cr = count($row);
            if ($ch != $cr && strlen(trim($row[$cr-1]))==0)
                unset($row[$cr-1]);
            #echo count($this->header);
            #print_r($row);
            if(count($this->header)!=count($row))
                print_r($row);
            $this->rowid++;
            $tmp[] = array_combine($this->header, $row);
            $tmp[count($tmp)-1]['@row'] = $this->rowid;
        }
        //$this->row = $row;
        //return array();
        return $tmp;
    }

    public function lookup($reference, $ref=null)
    {
        $rr = str_replace(" ", "_", $reference);
        #print_r($ref);
        //echo $rr, $ref[$rr], array_key_exists($rr, $ref), "\n";
        if(array_key_exists($rr, $ref))
            return $ref[$rr];
        else return null;
    }
}

class XmlSource extends RmlSource
{
    private $xml;

    public function __construct()
    {
        
    }

    public function open($location)
    {
        //echo $location, file_exists($location);
        $this->xml = simplexml_load_string(get_content($location, ".xml"));
        //$this->xml = simplexml_load_string(file_get_contents($location));
        $this->_handleDefaultNS($this->xml);
    }

    public function iterate($iterator, $ref=null)
    {
        return $this->xml->xpath($iterator);
    }

    private function _handleDefaultNS($ref)
    {
        $ns = $ref->getDocNamespaces();
        $strPrefix = '_';
        if (array_key_exists("", $ns))
        {
            $ref->registerXPathNamespace($strPrefix,$ns['']);
        }    
    }

    public function lookup($reference, $ref=null)
    {
        if($ref)
        {
            $this->_handleDefaultNS($ref);
            return @$ref->xpath($reference);
        }
        else
            return $this->xml->xpath($reference);
    }
}

class HTMLXPathSource extends RmlSource
{
    private $dom;
    private $xpath;

    public function __construct()
    {
        libxml_use_internal_errors(true);
        $this->dom = new DomDocument;
    }

    public function open($location)
    {
        $this->dom->loadHTML(get_content($location, ".html"));
        $this->xpath = new DomXPath($this->dom);
    }

    public function iterate($iterator, $ref=null)
    {
        return $this->xpath->query($iterator);
    }

    public function lookup($reference, $ref=null)
    {
        if($ref)
        {
            $o =  $this->xpath->evaluate($reference, $ref);
        }
        else
            $o = $this->xpath->query($reference);
        #echo $reference; print_r($o);
        if(is_a($o, 'DOMNodeList')) 
        {
            if($o->length == 1)
                $o = $o->item(0)->nodeValue;
            else
                return "";
        }
        return $o;
    }
}

class BibtexSource extends RmlSource
{
    private $bib;
    private $obj;
    private $json;

    public function __construct()
    {
        $this->bib = new Structures_BibTex();
        $this->json = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
    }

    public function open($location)
    {
        $this->bib->load(get_content($location, ".bib"));
        $this->bib->parse();
    }

    public function iterate($iterator, $ref=null)
    {
        $tmp = jsonPath($this->json->decode($this->json->encode($this->bib->data)), $iterator);
        if($tmp==false)
            return $this->bib->data;
        else
            return $tmp;
    }

    public function lookup($reference, $ref=null)
    {
        if ($ref==null || $ref===false)
            return jsonPath($this->bib->data, $reference);
        else
            return jsonPath($ref, $reference);
        /*
        if(array_key_exists($reference, $ref))
            return $ref[$reference];
        else return null;
        */
    }
}

function col2int($col)
{
    //echo "\n";
    $c = 0;
    for($i=0;$i<strlen($col);++$i)
    {
        $c += 1+ord(strtoupper($col[$i]))-ord('A');
        //echo $col[$i], $c, "\n";
        if($i<strlen($col)-1) $c *= 26;
    }
    return $c-1;
}



class SpreadsheetSource extends RmlSource
{
    private $reader;
    private $sheets;
    private $sheet;
    private $dcellstart; 
    private $dcellstop; 
    private $hcellstart; 
    private $hcellstop; 
    private $header;

    public function __construct()
    {
    }

    public function __destruct()
    {
    }

    public function open($location)
    {
        /*
        global $content_cache;
        if(!array_key_exists($location, $content_cache))
        {
            $src_path = "tmp/";
            $ext = pathinfo($location, PATHINFO_EXTENSION);

            $content = file_get_contents($location);
            $rml_file = $src_path.md5($content).".".$ext;
            if(!file_exists($rml_file))
                file_put_contents($rml_file, $content);
            $content_cache[$location] = $rml_file;
        }*/
        $this->reader = new SpreadsheetReader(get_contentfile($location, '.'.pathinfo($location, PATHINFO_EXTENSION)));
        $this->sheets = $this->reader->Sheets();
        
        $MAX_CELL = 9999999;
        $this->hcellstart = array(0, 0);
        $this->dcellstart = array(0, 1);
        $this->hcellstop = array($MAX_CELL, 0);
        $this->dcellstop = array($MAX_CELL, $MAX_CELL);
    }

    protected function parseIterator($iterator)
    {
        //parse iterator name into sheetname and cell ranges (data and header)
        //iterator format : <sheetname>!<data-startcell>[:<data-endcell>[:<header-startcell>:<header-endcell>]]
        //example : Sheet2!B3:E5
        //for csv, sheetname could be any string
        $tokens = array();
        $tok = strtok($iterator, "!:");
        while ($tok !== false) {
            $tokens[] = $tok;
            $tok = strtok("!:");
        }
        #print_r($tokens);
        $this->sheet = $tokens[0];
        if(count($tokens)>1)
        {
            preg_match("/(\D+)(\d+)/", $tokens[1], $matches);
            $this->dcellstart = array(col2int($matches[1]), intval($matches[2])-1);

            if(count($tokens)>2)
            {
                preg_match("/(\D+)(\d+)/", $tokens[2], $matches);
                $this->dcellstop = array(col2int($matches[1]), intval($matches[2])-1);    
            }
            
            if(count($tokens)>4)
            {
                preg_match("/(\D+)(\d+)/", $tokens[3], $matches);
                //print_r($matches);
                $this->hcellstart = array(col2int($matches[1]), intval($matches[2])-1);
                preg_match("/(\D+)(\d+)/", $tokens[4], $matches);
                //print_r($matches);
                $this->hcellstop = array(col2int($matches[1]), intval($matches[2])-1);
            }
            else
            {
                //first row from dcellstart-dcellstop
                $this->hcellstart = array($this->dcellstart[0], $this->dcellstart[1]);
                $this->hcellstop = array($this->dcellstop[0], $this->dcellstart[1]);
                $this->dcellstart = array($this->dcellstart[0], $this->dcellstart[1]+1);
            }
        }
        //print_r($this->dcellstart);
        //print_r($this->dcellstop);
        //print_r($this->hcellstart);
        //print_r($this->hcellstop);
        //if cell range is given, check also for header range
        //if header-cells range is not given then the header is assumed to be on the first row of data-cells range   
    }

    public function iterate($iterator, $ref=null)
    {
        if(strlen($iterator)>0)
        {
            $this->parseIterator($iterator);
            $this->reader->ChangeSheet(array_search($this->sheet, $this->sheets));
        }
        
        //$this->reader->rewind()
        $tmp = array();
        $row = 0;
        $this->header = array('@id');
        //echo var_export($this->header);
        foreach($this->reader as $rowdata)
        {
            if($row>=$this->hcellstart[1] && $row<=$this->hcellstop[1])
            {
                //array_merge($this->header, $this->reader->current());
                //array_merge($this->header, $rowdata);
                //echo $row, var_export($this->header), var_export($rowdata), "\n";
                //foreach($this->header as $idx => $head)
                foreach($rowdata as $idx => $head)
                {
                    if(strlen(trim($head))>0)
                        $this->header[] = str_replace(" ", "_", $head);
                }

                //echo $row, var_export($this->header), var_export($rowdata), "\n";
            }
            //echo count($this->header), count($rowdata), "\n";
            if($row>=$this->dcellstart[1] && $row<=$this->dcellstop[1])
            {
                $newrow = array($row-$this->dcellstart[1]);
                foreach($rowdata as $idx => $head)
                {
                    if($idx<count($this->header)-1)
                        $newrow[] = str_replace(" ", "_", $head);
                }
                //var_dump($newrow);
                //echo var_export($newrow), "\n";break;
                //array_merge(array($row-$this->dcellstart[1]), $rowdata);
                $tmp[] = array_combine($this->header, $newrow);
            }
            $row++;
        }
        return $tmp;
    }

    public function lookup($reference, $ref=null)
    {
        $rr = str_replace(" ", "_", $reference);
        #print_r($ref);
        #echo $rr, $ref[$rr], array_key_exists($rr, $ref), "\n";
        if(array_key_exists($rr, $ref))
            return $ref[$rr];
        else return null;
    }
}

class SpreadsheetPivotSource extends RmlSource
{
    private $reader;
    private $sheets;
    private $sheet;
    private $dcellstart; 
    private $dcellstop; 
    private $hcellstart; 
    private $hcellstop; 
    private $header;
    private $pivot;

    public function __construct()
    {
    }

    public function __destruct()
    {
    }

    public function open($location)
    {
        /*
        global $content_cache;
        if(!array_key_exists($location, $content_cache))
        {
            $src_path = "tmp/";
            $ext = pathinfo($location, PATHINFO_EXTENSION);

            $content = file_get_contents($location);
            $rml_file = $src_path.md5($content).".".$ext;
            if(!file_exists($rml_file))
                file_put_contents($rml_file, $content);
            $content_cache[$location] = $rml_file;
        }*/
        $this->reader = new SpreadsheetReader(get_contentfile($location, '.'.pathinfo($location, PATHINFO_EXTENSION)));
        $this->sheets = $this->reader->Sheets();
        
        $MAX_CELL = 9999999;
        $this->hcellstart = array(0, 0);
        $this->dcellstart = array(1, 1);
        $this->ccellstart = array(0,1);
        $this->ccellstop = array(0, $MAX_CELL);
        $this->hcellstop = array($MAX_CELL, 0);
        $this->dcellstop = array($MAX_CELL, $MAX_CELL);
    }

    protected function parseIterator($iterator)
    {
        //parse iterator name into sheetname and cell ranges (data and header)
        //iterator format : <sheetname>!<data-startcell>[:<data-endcell>[:<header-startcell>:<header-endcell>]]
        //example : Sheet2!B3:E5
        //for csv, sheetname could be any string
        $tokens = array();
        $tok = strtok($iterator, "!:");
        while ($tok !== false) {
            $tokens[] = $tok;
            $tok = strtok("!:");
        }
        #print_r($tokens);
        $this->sheet = $tokens[0];
        if(count($tokens)>1)
        {
            preg_match("/(\D+)(\d+)/", $tokens[1], $matches);
            $this->dcellstart = array(col2int($matches[1]), intval($matches[2])-1);
            #var_dump($matches);
            #if(count($tokens)>2)
            #{
                preg_match("/(\D+)(\d+)/", $tokens[2], $matches);
                $this->dcellstop = array(col2int($matches[1]), intval($matches[2])-1);    
            #}
            #var_dump($matches);
            #var_dump($this->dcellstart);
            #var_dump($this->dcellstop);

            if(count($tokens)>4)
            {
                preg_match("/(\D+)(\d+)/", $tokens[3], $matches);
                //print_r($matches);
                $this->hcellstart = array(col2int($matches[1]), intval($matches[2])-1);
                preg_match("/(\D+)(\d+)/", $tokens[4], $matches);
                //print_r($matches);
                $this->hcellstop = array(col2int($matches[1]), intval($matches[2])-1);
            }
            else
            {
                //first row from dcellstart-dcellstop
                $this->hcellstart = array($this->dcellstart[0], $this->dcellstart[1]);
                $this->hcellstop = array($this->dcellstop[0], $this->dcellstart[1]);
                $this->dcellstart = array($this->dcellstart[0], $this->dcellstart[1]+1);
            }
                $this->ccellstart = array($this->dcellstart[0]-1, $this->dcellstart[1]);
                $this->ccellstop = array($this->ccellstart[0], $this->dcellstop[1]);
                #var_dump($this->ccellstart);
                #var_dump($this->ccellstop);
        }
        //print_r($this->dcellstart);
        //print_r($this->dcellstop);
        //print_r($this->hcellstart);
        //print_r($this->hcellstop);
        //if cell range is given, check also for header range
        //if header-cells range is not given then the header is assumed to be on the first row of data-cells range   
    }

    public function iterate($iterator, $ref=null)
    {
        /*
         * the difference between pivot and traditional spreadsheet is that in pivot, each record is based on each cell
         * instead of row-based record in the traditional spreadsheet
         */
        if(strlen($iterator)>0)
        {
            $this->parseIterator($iterator);
            $this->reader->ChangeSheet(array_search($this->sheet, $this->sheets));
        }
        
        //$this->reader->rewind()
        $tmp = array();
        $row = 0;
        $this->header = array('@id');
        $this->pivot_column = array();
        //echo var_export($this->header);
        $itemctr = 1;
        foreach($this->reader as $rowdata)
        {
            if($row>=$this->hcellstart[1] && $row<=$this->hcellstop[1])
            {
                //array_merge($this->header, $this->reader->current());
                //array_merge($this->header, $rowdata);
                //echo $row, var_export($this->header), var_export($rowdata), "\n";
                //foreach($this->header as $idx => $head)
                #var_dump($rowdata);
                $colctr = 0;
                foreach($rowdata as $idx => $head)
                {
                    #if(strlen(trim($head))>0)
                        #$this->pivot[] = str_replace(" ", "_", $head);
                    if($colctr==$this->hcellstart[0])
                    {
                        $this->header[] = $head;
                        $this->header[] = "@pivot";
                        $this->header[] = "@value";
                    }
                    else if($colctr>$this->hcellstart[0] && $colctr<=$this->hcellstop[0])
                        $this->pivot[] = $head;

                    $colctr++;
                }

                //echo $row, var_export($this->header), var_export($rowdata), "\n";
            }
            //echo count($this->header), count($rowdata), "\n";
            if($row>=$this->dcellstart[1] && $row<=$this->dcellstop[1])
            {
                #$newrow = array($row-$this->dcellstart[1]);
                $colctr = 0;
                $fixed = "";
                foreach($rowdata as $idx => $head)
                {
                    if($colctr==$this->ccellstart[0])
                    {
                        $fixed = $head;
                    }
                    else if($colctr>=$this->dcellstart[0] && $colctr<=$this->dcellstop[0])
                    {
                        $newrow = array($itemctr);
                        $itemctr++;
                        $newrow[] = $fixed;
                        $newrow['@pivot'] = $this->pivot[$colctr-$this->dcellstart[0]];
                        $newrow['@value'] = $head;
                        $tmp[] = array_combine($this->header, $newrow);
                        #var_dump($newrow);
                    }
                    #if($idx<count($this->header)-1)
                        #$newrow[] = str_replace(" ", "_", $head);
                    $colctr++;
                }
                //var_dump($newrow);
                //echo var_export($newrow), "\n";break;
                //array_merge(array($row-$this->dcellstart[1]), $rowdata);
                #$tmp[] = array_combine($this->header, $newrow);
            }
            $row++;
        }
        #var_dump($this->header);
        #echo count($this->pivot);
        #echo count($tmp);
        return $tmp;
    }

    public function lookup($reference, $ref=null)
    {
        $rr = str_replace(" ", "_", $reference);
        #print_r($ref);
        #echo $rr, $ref[$rr], array_key_exists($rr, $ref), "\n";
        if(array_key_exists($rr, $ref))
            return $ref[$rr];
        else return null;
    }
}

function map_vars($template, $values)
{
    $vars = extract_vars($template);
    $out = $template;
    foreach($vars as $k => $var){
        $evar = (substr($var,0,1)=='!')?substr($var, 1):$var;
        if(array_key_exists($var, $values))
        {
            $val = $values[$var];    
            if(strlen($evar)==strlen($var))
                $out = str_replace("{".$var."}", urlencode($val), $out);
            else
                $out = str_replace("{".$var."}", $val, $out);//raw values
        }
        else
        {
            return "";
        }
    }
    return $out;
}

function lookup_source($src, $key, $ref, $vars)
{
    if(strlen($key)==0)
        return "";

    if(array_key_exists($key, $vars))
        return $vars[$key];
    else if(is_array($src) && array_key_exists($key, $src))
        return $src[$key];
    else if($src != null)
        return $src->lookup($key, $ref);
    else 
        return "";
}

function sort_transform_blocks($triplemap)
{
    $varmap = $triplemap->all("rmlx:transform");

    $ovarmap = array();
    $rovar = array();
    $rsovar = array();
    $vardep = array();
    //reorder variable mapping based on variable dependency
    foreach($varmap as $kvar => $varnode)
    {
        $ovar = $varnode->get("rmlx:outputVar")->getValue();
        $rovar[$ovar] = $varnode;
        $rsovar[$ovar] = array();
        $vardep[$ovar] = 0;
    }
    //TODO:array ordering is wrong, weight should be recursive
    foreach($varmap as $kvar => $varnode)
    {
        $ovar = $varnode->get("rmlx:outputVar")->getValue();
        $param = $varnode->all("rmlx:parameterMapping");
        foreach($param as $pi => $pnode)
        {
            $pname = $pnode->get("rmlx:varName");
            if($pname)
                $pname = $pname->getValue();
            $pref = $pnode->get("rml:reference");
            $ptpl = $pnode->get("rr:template");
            if($pref)
            {
                $pname = $pref->getValue();
                if(strpos($pname, "[")>0) $pname = substr($pname, 0, strpos($pname, "["));
            }

            if(array_key_exists($pname, $rsovar))
                $rsovar[$ovar][] = $pname;

            if($ptpl)
            {
                $pvars = extract_vars($ptpl);
                $unames = array();
                foreach($pvars as $l => $rname)
                {
                    if(strpos($rname, "[")>0) 
                        $rname = substr($rname, 0, strpos($rname, "["));
                    if(!array_key_exists($rname, $unames))
                        $unames[$rname] = 1;
                }
                foreach($unames as $pname => $l)
                {
                    //$vardep[$pname] = $vardep[$pname] + 1;
                    $rsovar[$ovar][] = $pname;
                }
            }
            
        }
    }
    foreach($rsovar as $k => $v)
    {
        $vardep[$k] = _vdepth($k, $rsovar);
    }
    asort($vardep);
    foreach($vardep as $k => $v)
    {
        $ovarmap[] = $rovar[$k];
    }
    return $ovarmap;
}

function process_transform_blocks($varmap, $reader, $iterobj, &$poolvars)
{
    global $api_cache, $mapper;
    foreach($varmap as $kvar => $varnode)
    {
        $ovar = $varnode->get("rmlx:outputVar");
        $op = $varnode->get("rmlx:function");
        $param = $varnode->all("rmlx:parameterMapping");
        $paramVars = array();
        foreach($param as $pi => $pnode)
        {
            $pname = $pnode->get("rmlx:varName");
            if($pname)
                $pname = $pname->getValue();
            $pconst = $pnode->get("rr:constant");
            $pref = $pnode->get("rml:reference");
            $ptemp = $pnode->get("rr:template");
            $rest = $pnode->propertyUris();   
            if($pconst != NULL) 
                $paramVars[$pname] = $pconst->getValue();
            else if($pref != NULL)
            { 
                $vals = lookup_source($reader, $pref->getValue(), $iterobj, $poolvars);
                if(is_array($vals))
                    $vals = $vals[0];
                $paramVars[$pname] = $vals;
            }
            else if($ptemp)
            {
                $tvars = extract_vars($ptemp);
                $tmp = $ptemp;
                foreach($tvars as $k => $var){
                    $val = lookup_source($reader, $var, $iterobj, $poolvars);
                    if(is_array($val))
                        $val = $val[0];
                    $tmp = str_replace("{".$var."}", $val, $tmp);
                }
                $pval = $tmp;     
                $paramVars[$pname] = $pval;
            }
            else $paramVars[$pname] = "";
        }

        if(!$op->isBnode())
        {
            #print_r($op->getUri());
            switch($op->getUri())
            {
                case _exp("rop:replace"):
                    #var_dump($paramVars);
                    $poolvars[$ovar->getValue()] = str_replace($paramVars["search"], $paramVars["replace"], $paramVars["subject"]);
                break;

                case _exp("rop:regex-replace"):
                    $poolvars[$ovar->getValue()] = preg_replace($paramVars["search"], $paramVars["replace"], $paramVars["subject"]);;
                break;

                case _exp("rop:uppercase"):
                    $poolvars[$ovar->getValue()] = strtoupper($paramVars["input"]);
                break;

                case _exp("rop:lowercase"):
                    $poolvars[$ovar->getValue()] = strtolower($paramVars["input"]);
                break;

                case _exp("rop:trim"):
                    $poolvars[$ovar->getValue()] = trim($paramVars["input"]);
                break;

                case _exp("rop:assign"):
                    $poolvars[$ovar->getValue()] = $paramVars["value"];
                break;

                case _exp("rop:make_array"):
                    $poolvars[$ovar->getValue()] = array();
                break;

                case _exp("rop:array_push"):
                    $poolvars[$ovar->getValue()][] = $paramVars["value"];
                break;

                case _exp("rop:md5"):
                    $poolvars[$ovar->getValue()][] = md5($paramVars["value"]);
                break;

                case _exp("rop:sha1"):
                    $poolvars[$ovar->getValue()][] = sha1($paramVars["value"]);
                break;

                case _exp("rop:array_to_json"):
                    $poolvars[$ovar->getValue()] = json_encode($paramVars["input"]);
                break;

                case _exp("rop:reset_auto_increment"):
                    $aivar = "__AUTOINC__".$paramVars['value'];
                    $poolvars[$aivar] = 0;
                break;

                case _exp("rop:parseReference"):
                    //use-case https://opensas.wordpress.com/2013/06/30/using-openrefine-to-geocode-your-data-using-google-and-openstreetmap-api/
                    $ql = $pnode->get("rml:referenceFormulation");
                    if($ql == null)
                        $ql = $pnode->get("rml:queryLanguage");
                    $expr = $paramVars['selector'];
                    $format = $paramVars['mime_type'];
                    $data = $paramVars['data'];

                    $ref_reader = new $mapper[$ql->localName()]();
                    $ref_reader->open("data:".$format.",".$data);
                    $poolvars[$ovar->getValue()] = lookup_source($ref_reader, $expr, array(), $poolvars);
                break;

                case _exp("rop:auto_increment"):
                    $aivar = "__AUTOINC__".$paramVars['value'];
                    if(!array_key_exists($aivar, $poolvars))
                        $poolvars[$aivar] = 0;
                    $poolvars[$aivar] = $poolvars[$aivar] + 1;
                    #echo $aivar.' '.$poolvars[$aivar]."\n";
                    $poolvars[$ovar->getValue()] = $poolvars[$aivar];
                break;

                case _exp("rop:split"):
                    $opres = explode($paramVars['separator'], $paramVars["input"]);
                    foreach($opres as $k => $v)
                    {
                        $poolvars[$ovar->getValue()."[".$k."]"] = $v;
                        $poolvars[$ovar->getValue()."[".($k-count($opres))."]"] = $v;
                    }
                    #print_r($initVars);
                    $poolvars[$ovar->getValue()] = $opres;
                break;
            }

        }
        else
        {
            #print_r($op->getUri());
            //TODO:handle Blank Node (remote function/call API, SPIN Templates)    
            //REST API : uses HTTP vocabulary http://www.w3.org/2011/http#
            //http:resp will be stored inside the output variable
            $uri = $op->get("http:requestURI");
            $resp_fmt = $op->get("rmlx:responseFormulation");
            $resp_sel = $op->get("rmlx:responseSelector");
            #print_r("http:methodName);
            $optype = $op->get("rdf:type");
            if($optype == _exp("http:Request") || $uri)
            {
                $method = $op->get("http:methodName");
                if($method)
                    $method = $method->getValue();
                else
                    $method = "GET";

                $utpl = $uri->getValue();
                $tmp = $utpl;
                $params = array();
                $vars = extract_vars($utpl);
                if(count($vars)>0)
                {
                    $vars = $vars[1];
                    
                    $tmp = str_replace("{".$vars."}", "", $tmp);
                    if(strpos($vars, ',')>0)
                        $vars = explode(',', $vars);
                    else
                        $vars = array($vars);
                    foreach($vars as $k => $var){
                        if(array_key_exists($var, $paramVars))
                            $params[$var] = $paramVars[$var];
                    }

                }
                $uval = $tmp;
                #var_dump($uval);
                $api_hash = md5($uval.json_encode($params));
                #print_r($api_hash);
                if(!array_key_exists($api_hash, $api_cache))
                {
                    $tmp = make_request($uval, $params, $method);
                    if(strlen($tmp)>0)
                        print_r("#".$tmp);
                    else
                        echo "#".json_encode($params);
                    echo "\n";
                    $api_cache[$api_hash] = $tmp;
                    if($resp_fmt)
                    {
                        
                    }
                }
                $poolvars[$ovar->getValue()] = $api_cache[$api_hash];
            }
        }
        
    }
}

function triplemap_vars($tmap)
{
    //this function returns all variables generated in this triplemap
    //using path: $tmap rmlx:transform/rmlx:outputVar $varname
    $result = array();
    $xform = $tmap->all("rmlx:transform");
    foreach($xform as $k => $varnode)
    {
        $result[] = $varnode->get("rmlx:outputVar")->getValue();
    }
    return $result;
}

function triplemap_sourcetemplate($tmap)
{
    //this function returns all variables used in the sourceTemplate (if any)
    //using path: $tmap rml:logicalSource/rmlx:sourceTemplate $tpl
    $ls = $tmap->get("rml:logicalSource");
    if($ls)
    {
        $tpl = $ls->get("rmlx:sourceTemplate");
        if($tpl)
        {
            return extract_vars($tpl->getValue());
        }
    }
    return array();
}

function termmap_inputvars($tm)
{
    $result = array();
    $tpl = $tm->get("rr:template");
    $tr = $tm->get("rml:reference");
    if($tpl)
    {
        $result = array_merge($result, extract_vars($tpl));
    }
    else if($tr)
    {
        $result[] = $tr->getValue();
    }
    return $result;
}

function triplemap_depvars($tmap)
{
    //this function returns all dependent variables used in every occurence of rr:template or rml:reference
    //using path: $tmap rml:logicalSource/rmlx:sourceTemplate $tpl (sourceTemplate)
    //$tmap rml:subjectMap/rr:template $tpl
    //$tmap rml:subjectMap/rml:reference $ref
    //$tmap rml:predicateObjectMap/rml:predicateMap/rr:template $tpl
    //$tmap rml:predicateObjectMap/rml:predicateMap/rml:reference $ref
    //$tmap rml:predicateObjectMap/rml:objectMap/rr:template $tpl
    //$tmap rml:predicateObjectMap/rml:objectMap/rml:reference $ref
    $result = array();
    $ls = $tmap->get("rml:logicalSource");
    if($ls)
    {
        $tpl = $ls->get("rmlx:sourceTemplate");
        if($tpl)
        {
            $result = array_merge($result, extract_vars($tpl->getValue()));
        }
    }
    
    $tm = $tmap->get("rr:subjectMap");
    if($tm)
        $result = array_merge($result, termmap_inputvars($tm));
    foreach($tmap->all("rr:predicateObjectMap") as $k => $pomap)
    {
        $tm = $tmap->get("rr:predicateMap");
        if($tm)
            $result = array_merge($result, termmap_inputvars($tm));
        $tm = $tmap->get("rr:objectMap");
        if($tm)
            $result = array_merge($result, termmap_inputvars($tm));
    }
    return array_unique($result);
}

function triplemap_defaultvalues($g, &$ivars)
{
    //this function returns all variables used in the sourceTemplate (if any)
    //using path: $tmap rml:logicalSource/rmlx:sourceTemplate $tpl
    foreach($g->resourcesMatching("rmlx:defaultValue") as $k => $pdef)
    {
        foreach($pdef->all("rmlx:defaultValue") as $_k => $pnode)
        {
            //var_dump($pnode);
            $pname = $pnode->get("rmlx:varName")->getValue();
            $pconst = $pnode->get("rr:constant");
            $ptemp = $pnode->get("rr:template");
            if(array_key_exists($pname, $ivars)) continue;
            if($pconst != NULL)
            {
                    $ivars[$pname] = $pconst->getValue();
            }
            else if($ptemp)
            {
                $tvars = extract_vars($ptemp);
                $tmp = $ptemp;
                foreach($tvars as $k => $var){
                    $val = lookup_source($reader, $var, $iterobj, $poolvars);
                    if(is_array($val))
                        $val = $val[0];
                    $tmp = str_replace("{".$var."}", $val, $tmp);
                }
                $pval = $tmp;     
                $ivars[$pname] = $pval;
            }    
        }
        
    }
}

function process_termmap($tmap, $iterator, $ref, $initVars)
{
    $pconst = $tmap->get("rr:constant");//constant iri is a string and converted into IRI
    $ptpl = $tmap->get("rr:template");
    $pref = $tmap->get("rml:reference");
    $pval = "";

    if($pconst)
    {
        $pval = $pconst->getValue();
    }
    else if($pref)
    {
        $pval = lookup_source($format_reader, $pref, $ref, $initVars);
        if(is_array($pval))
            if(count($pval)>0)
                $pval = $pval[0];
    }
    else if($ptpl)
    {
        $tvars = extract_vars($ptpl);
        $tmp = $ptpl;
        foreach($tvars as $k => $var){
            $val = lookup_source($format_reader, $var, $ref, $initVars);
            if(is_array($val))
                $val = $val[0];
            $tmp = str_replace("{".$var."}", $val, $tmp);
        }
        $pval = $tmp;
    }
    return $pval;
}

$mapper = array(
        'JSONPath'=>'JsonSource', 
        'CSV'=>'CSVSource', 
        'XPath'=>'XmlSource', 
        'HTMLXPath'=>'HTMLXPathSource', 
        'CSS'=>'CSSSource', 
        'Bibtex'=>'BibtexSource', 
        'Spreadsheet'=>'SpreadsheetSource', 
        'Pivot'=>'SpreadsheetPivotSource', 
        'RegExp'=>'RegExpSource');

function process_triplemap($map, $output, &$initVars, &$lookup, $dependency)
{
    $src_path = "tmp/";
    global $mapper;

    $stop = false;//stop processing this TriplesMap

    $mapuri = $map->getUri();
    //print_r($mapuri);
    $ls = $map->get("rml:logicalSource");
    $lt = $map->get("rr:logicalTable");
    //if(!$ls) echo _exp("rml:logicalSource");
    if($ls)
    {
        //print_r($map->getUri());
        //$sourceName = $ls->get("rml:sourceName");
        //check for sourceTemplate
        $sourceTemplate = $ls->get("rmlx:sourceTemplate");
        //var_dump($sourceTemplate);
        if($sourceTemplate)
        {
            $sourceTemplate = $sourceTemplate->getValue();
            $vars = extract_vars($sourceTemplate);
            $sourceName = map_vars($sourceTemplate, $initVars);
        }
        else
        {
            $sourceName = $ls->get("rml:source");
            if($sourceName == null)
                $sourceName = $ls->get("rml:sourceName");
            $sourceName = $sourceName->getValue();
        }
        //2016-04-28
        //RMLProcessor use the same predicate for source containing uri template
        $vars = extract_vars($sourceName);
        if(count($vars)>0)
        {
            $sourceName = map_vars($sourceName, $initVars);
        }

        //echo $sourceName;
        
        //$ql = $ls->get("rml:queryLanguage");

        $ql = $ls->get("rml:referenceFormulation");
        if($ql == null)
            $ql = $ls->get("rml:queryLanguage");

        $iterator = $ls->get("rml:iterator");
        if($iterator == null)
            $iterator = $ls->get("rml:separator");
        if($iterator == null && $ql->localName() == 'CSV')
            $iterator = ",";
        
        if(is_a($iterator, 'EasyRdf_Literal'))
            $iterator = $iterator->getValue();
        
        
        if(!array_key_exists($ql->localName, $mapper))
        {
            //how to interpret unrecognized localname, custom format? what is the serialization format? JSON? RDF?
            //TODO: decide and implement
        }

        $format_reader = new $mapper[$ql->localName()]();
        if(substr( $sourceName, 0, 4 ) === "http" || substr( $sourceName, 0, 4 ) === "data")
            $format_reader->open($sourceName);
        else
            $format_reader->open($src_path.$sourceName);//local dir
        
        $collection = $format_reader->iterate($iterator);
        //echo $sourceName, " ", $iterator," ", $ql->localName(), " ", count($collection), "\n";
        //echo print_r($collection);
    }
    else if ($lt)
    {
        //TODO: finish/test this part
        $sourceTemplate = $ls->get("rmlx:sourceTemplate");
        if($sourceTemplate != null)
        {
            $sourceTemplate = $sourceTemplate->getValue();
            $vars = extract_vars($sourceTemplate);
            $sourceName = map_vars($sourceTemplate, $initVars);
        }
        else
        {
            $sourceName = $ls->get("rml:source");
            if($sourceName == null)
                $sourceName = $ls->get("rml:sourceName");
            $sourceName = $sourceName->getValue();
        }

        $ql = $lt->get("rr:sqlVersion");
        $tn = $lt->get("rr:tableName");
        $iterator = $lt->get("rr:sqlQuery");

        if(is_a($iterator, 'EasyRdf_Literal'))
            $iterator = $iterator->getValue();

        $format_reader = new PDOSource();
        time_elapsed();
        $format_reader->open($sourceName);
        $collection = $format_reader->iterate($iterator);
        time_elapsed();
    }
    else
    {
        //print_r("no logical source\n");
        $collection = null;
        $format_reader = null;
    }

    //-- begin variable handling

    $ovarmap = sort_transform_blocks($map);
    
    //-- end var

    //-- begin subject handling
    $scount = 0;
    $_subj = $map->get("rr:subject");
    $smap = $map->get("rr:subjectMap");

    if($_subj)
    {
        if($collection === null)
            $collection = array(array());
        $stpl = null;
        $class = null;
        $sconst = null;
    }
    else if($smap)
    {
        if($collection === null)
            $collection = array(array());
        $sconst = $smap->get("rr:constant");
        $class = $smap->all("rr:class");
        $stpl = $smap->get("rr:template");    
    }
    else
    {
        //ERROR: no subjectmap defined
        process_transform_blocks($ovarmap, $format_reader, array(), $initVars);
        $stop = true;
        return;
    }

    //-- end subject handling

    $_pred = $map->all("rr:predicate");
    $_ipred = $map->all("rmlx:predicateInv");
    $pomap = $map->all("rr:predicateObjectMap");

    if($_pred)
    {
        $pomap = array($map);
    }

    $_obj = $map->get("rr:object");

    
    #print_r($collection);
    #var_dump($collection);
    foreach($collection as $k => $obj)
    {
        //transformation blocks executed inside a row loop
        process_transform_blocks($ovarmap, $format_reader, $obj, $initVars);
        #print_r($initVars);
        if($_subj)
        {
            //rr:subject
            $subj = $_subj;
        }
        else 
        {
            //rr:subjectMap
            $sconst = $smap->get("rr:constant");
            $stpl = $smap->get("rr:template");
            $sttype = $smap->get("rr:termType");
            
            if($sconst)
            {
                $subj = $sconst;
            }
            else if ($sttype && $sttype==_exp("rr:BlankNode"))
            {
                $subj = $output->newBNode();
            }
            else if($stpl){
                
                $vars = extract_vars($stpl);
                
                $url = $stpl;
                $allempty = true;
                foreach($vars as $k => $var){
                    $evar = $var;
                    
                    if(strpos($var, '!')===0)
                        $var = substr($var, 1);
                    $val = lookup_source($format_reader, $var, $obj, $initVars);

                    //print_r($val);
                    if(is_array($val))
                        if(count($val)>0)
                            $val = $val[0];
                        else
                            $val = "";
                    #echo $var." = ".$val."\n";    
                    $allempty &= strlen($val)==0;
                    if($evar != $var)
                        $url = str_replace("{".$evar."}", $val, $url);    
                    else
                        $url = str_replace("{".$var."}", urlencode($val), $url);
                }
                if($allempty){$scount += 1; continue;}
                #echo $url;
                $subj = $output->resource($url);
            }
            else
            {
                //rr:subject shortcut
                $subj = $obj;
            }
            
            $class = $smap->all("rr:class");
            if($class){
                foreach($class as $c => $cls)
                {
                    //2016-04-28
                    //bugfix if string is put into rr:constant on rr:subjectMap, then it should be converted into IRI
                    if(is_a($subj, 'EasyRdf_Literal')) $subj = $output->resource($subj->getValue());
                    $output->add($subj, "rdf:type", $cls);    
                }
            }

        }
        $subj_url = $subj->getUri();

        if(array_key_exists($mapuri, $dependency))
        {
            foreach($dependency[$mapuri] as $kd => $depmap)
            {
                $tmp = array();
                $mapping = array();
                $multi = false;
                $keys = array();
                foreach($depmap[0] as $kk => $depkey)
                {
                    $keys[] = $depkey;
                    $val = lookup_source($format_reader, $depkey, $obj, $initVars);
                    if(is_array($val))
                        if(count($val)==1)
                            $val = "".$val[0];
                        else
                            $multi = true;
                    
                    //if(is_array($val))
                    //    $val = $val[0];
                    //$mapping[$depkey] = "".$val;
                    $mapping[$depkey] = $val;
                }
                if($multi){
                    //print_r($mapping);
                    $firstkey = $keys[0];
                    //echo $firstkey, "\n";
                    foreach($mapping[$firstkey] as $ik => $iv)
                    {
                        $tmap = array();
                        foreach($depmap[0] as $kk => $depkey)
                        {
                            $tmap[$depkey] = $mapping[$depkey][$ik];
                        }
                        $tmp = array();
                        $tmp[] = $tmap;
                        $tmp[] = $subj_url;
                        //print_r($tmp);
                        $lookup[$mapuri][$depmap[1]][] = $tmp;
                    }
                }
                else{
                    $tmp[] = $mapping;
                    $tmp[] = $subj_url;
                    $lookup[$mapuri][$depmap[1]][] = $tmp;
                }
            }
        }

        foreach($pomap as $kp => $kv)
        {
            $value = "";
            $pred = $kv->get("rr:predicate");
            $ipred = $kv->get("rmlx:predicateInv");
            $pmap = $kv->get("rr:predicateMap");
            
            if($pred)
            {
                //OK
            }
            else if($ipred)
            {
                //OK echo $ipred->getUri();
            }    
            else if($pmap)
            {
                //override $pred with term created in this map
                $pconst = $pmap->get("rr:constant");//constant iri is a string and converted into IRI
                $ptpl = $pmap->get("rr:template");
                $pref = $pmap->get("rml:reference");
                $ptype = $pmap->get("rmlx:predicateType");

                $pval = "";

                if($pconst)
                {
                    $pval = $pconst->getValue();
                }
                else if($pref)
                {
                    $pval = lookup_source($format_reader, $pref, $obj, $initVars);
                    if(is_array($pval))
                        if(count($pval)>0)
                            $pval = $pval[0];
                }
                else if($ptpl)
                {
                    $tvars = extract_vars($ptpl);
                    $tmp = $ptpl;
                    foreach($tvars as $k => $var){
                        $val = lookup_source($format_reader, $var, $obj, $initVars);
                        if(is_array($val))
                            $val = $val[0];
                        $tmp = str_replace("{".$var."}", $val, $tmp);
                    }
                    $pval = $tmp;
                }
                if($ptype==_exp("rmlx:InversePredicate"))
                    $ipred = $output->resource($pval);
                else
                    $pred = $output->resource($pval);
            }
            else
            {
                //ERROR:no rr:predicate or rmlx:predicateInv or rr:predicateMap is given
                print_r("error no predicate inside predicateobjectmap\n");
                $stop = true;
                continue;    
            }

            
            $_obj = $kv->get("rr:object");
            #echo $pred->getUri(), " ";
            if($_obj)
            {
                //echo $_obj->getUri();
                $ovalue = $_obj;
                $const = null;
                $otpl = null;
                $oref = null;
                $dtype = null;
                $lang = null;
                $join = null;
                $ttype = null;
            }
            else
            {
                $omap = $kv->get("rr:objectMap");

                $const = $omap->get("rr:constant");
                $otpl = $omap->get("rr:template");
                $oref = $omap->get("rml:reference");
                $dtype = $omap->get("rr:datatype");
                $ttype = $omap->get("rr:termType");
                if($ipred) $ttype = _exp("rr:IRI");//20151109
                $lang = $omap->get("rr:language");
                $join = $omap->get("rr:parentTriplesMap");
                //introduce transformation here
                $ovalue = null;
            
            
                if($const)
                {
                    $ovalue = $const;
                }
                
                if($otpl)
                {
                    #echo "tpl:", $otpl, " ";
                    $tvars = extract_vars($otpl);
                    #var_dump($tvars);
                    $tmp = $otpl;
                    foreach($tvars as $k => $var){
                        $evar = $var;
                        if($ttype && $ttype==_exp("rr:IRI"))
                        {
                            if(strpos($var, '!')===0)
                                $var = substr($var, 1);
                        }
                        #var_dump($var, $evar);
                        $val = lookup_source($format_reader, $var, $obj, $initVars);
                        #var_dump($val);
                        if(is_array($val))
                        {
                            //var_dump($val);
                            if(count($val)==1)
                                $val = $val[0];
                            else
                                $val = "";
                        }
                        #var_dump($ttype && $ttype==_exp("rr:IRI") && strlen($evar)!=strlen($var));
                        if($ttype && $ttype==_exp("rr:IRI") && strlen($evar)!=strlen($var))
                            $tmp = str_replace("{".$evar."}", urlencode($val), $tmp);
                        else
                            $tmp = str_replace("{".$var."}", $val, $tmp);
                    }
                    $ovalue = $tmp;
                }
                
                if($oref)
                {
                    #echo "ref:", $oref, " ";
                    #print_r($oref);
                    if(is_object($oref))
                        $oref = $oref->getValue();
                    $val = lookup_source($format_reader, $oref, $obj, $initVars);
                    $ovalue = array();
                    if(is_array($val)){
                        foreach($val as $vv => $_v)
                            $ovalue[] = "".$_v;
                    }
                    else
                        $ovalue[] = $val;
                    #print_r($ovalue);
                }
                
                if($join)
                {
                    //echo "join: ";
                    $ttype = _exp("rr:IRI");
                    $jc = $omap->all("rr:joinCondition");
                    $jval = array();
                    foreach($jc as $k => $v)
                    {
                        $parent = $v->get("rr:parent");
                        $child = $v->get("rr:child")->getValue();
                        $val = lookup_source($format_reader, $child, $obj, $initVars);
                        if(is_array($val)){
                            $cval = array();
                            foreach($val as $vv => $_v) $cval[] = "".$_v;
                        }
                        else
                            $cval = "".$val;

                        $jval[$parent->getValue()] = $cval;
                    }

                    $ovalue = array();
                    foreach($lookup[$join->getUri()][$mapuri] as $k => $pv)
                    {
                        $match = true;
                        foreach($jval as $key => $kval)
                        {
                            $vmatch = false;
                            if(is_array($kval)){
                                foreach($kval as $kk => $kv)
                                    if($kv==$pv[0][$key]){
                                        $vmatch = true;
                                        break;
                                    }
                                if(!$vmatch){
                                    $match = false;
                                    break;
                                }
                            }
                            else
                                if(!array_key_exists($key, $pv[0]) || $pv[0][$key]!=$kval){
                                    $match = false;
                                    break;
                                }
                        }
                        if($match){
                            $ovalue[] = $pv[1];
                        }
                    }
                }
            }

            if ($ovalue==null || (is_string($ovalue) && strlen($ovalue)==0)) continue;
            if (is_array($ovalue) && count($ovalue)==1 && strlen($ovalue[0])==0) continue;

            if(!is_array($ovalue))
                $ovalue = array($ovalue);

            //TODO: check if there's a flag to handle multiple object into a structure (e.g. rdf:Seq)
            foreach($ovalue as $k => $value)
            {
                if($ttype)
                {
                    switch($ttype)
                    {
                        case _exp("rr:IRI"):
                            if(is_a($value, 'EasyRdf_Literal'))
                                $value = $value->getValue();
                            if(strlen(trim($value))==0) 
                                $value="#";
                            $value = $output->resource($value);
                            break; 

                        case _exp("rr:BlankNode"):
                            if(is_a($value, 'EasyRdf_Literal'))
                                $value = $value->getValue();
                            if(strlen(trim($value))==0) 
                                $value="#";
                            $value = $output->newBNode();
                            break;

                        case _exp("rr:Literal"):
                            if($dtype)
                            {
                                $value = EasyRdf_Literal::create($value, $lang, $dtype);
                            }
                            break;
                    }
                }
                else if($dtype)
                {
                    $value = EasyRdf_Literal::create($value, $lang, $dtype);
                }
                else
                {
                    if(!is_a($value, 'EasyRdf_Resource'))
                        $value = EasyRdf_Literal::create($value);
                }
                
                #echo "\t", $pred->getUri(), " ", $value,"\n";
                if($ipred)
                    $output->add($value, $ipred, $subj);
                else
                    $output->add($subj, $pred, $value);
            }
            
        }
    }
}

/*
EasyRdf_Namespace::set("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
EasyRdf_Namespace::set("rdfs", "http://www.w3.org/2000/01/rdf-schema#");
EasyRdf_Namespace::set("xsd", "http://www.w3.org/2001/XMLSchema#");
EasyRdf_Namespace::set("owl", "http://www.w3.org/2002/07/owl#");
*/
function force_namespaces()
{
EasyRdf_Namespace::set("rml", "http://semweb.mmlab.be/ns/rml#");
EasyRdf_Namespace::set("rr", "http://www.w3.org/ns/r2rml#");
EasyRdf_Namespace::set("ql", "http://semweb.mmlab.be/ns/ql#");
EasyRdf_Namespace::set("qb", "http://purl.org/linked-data/cube#");
EasyRdf_Namespace::set("geo", "http://www.w3.org/2003/01/geo/wgs84_pos#");
EasyRdf_Namespace::set("rmlx", "http://pebbie.org/ns/rmlx#");
EasyRdf_Namespace::set("qlx", "http://pebbie.org/ns/qlx#");
EasyRdf_Namespace::set("rop", "http://pebbie.org/ns/rmlx-functions#");
EasyRdf_Namespace::set("http", "http://www.w3.org/2011/http#");
EasyRdf_Namespace::set("sdmx-dimension", "http://purl.org/linked-data/sdmx/2009/dimension#");
EasyRdf_Namespace::set("sdmx-attribute", "http://purl.org/linked-data/sdmx/2009/attribute#");
EasyRdf_Namespace::set("sdmx-measure", "http://purl.org/linked-data/sdmx/2009/measure#");
}
force_namespaces();

function process_rml($rml_file, $iformat='turtle', $oformat='n3', $initVars=array())
{	
    $src_path = "tmp/";
	
    #print_r(EasyRdf_Namespace::namespaces());
    #echo EasyRdf_Namespace::expand("rml:logicalSource");
    $graph = new EasyRDF_Graph();
    time_elapsed();
    $graph->parse(file_get_contents($rml_file), $iformat);
    force_namespaces();
    $output = new EasyRDF_Graph();

    $dependency = array();
    $mapping = array();
    $tvars = array();
    triplemap_defaultvalues($graph, $initVars);      
    //echo $graph->serialise(EasyRdf_Format::getFormat("n3"));
    foreach($graph->resourcesMatching(_exp("rml:logicalSource")) as $key => $TripleMap)
    {
        $mapping[] = $TripleMap;
        foreach($graph->resourcesMatching(_exp("rr:parentTriplesMap"), $TripleMap) as $key => $JoinMap)
        {
            if($JoinMap != NULL)
            {
                $objmap = $graph->resourcesMatching(_exp("rr:objectMap"), $JoinMap);
                //print_r($objmap[0]);
                //break;
                $ChildMap = $graph->resourcesMatching(_exp("rr:predicateObjectMap"), $objmap[0]);
                //$ChildMap = $graph->resourcesMatching(_exp("rr:predicateObjectMap"), $graph->resourcesMatching(_exp("rr:objectMap"), $JoinMap)[0]);

                $jc = $JoinMap->all("rr:joinCondition");

                $parentRef = array();
                foreach($jc as $key => $joincond)
                {
                    $parentRef[] = $joincond->get("rr:parent")->getValue();
                }
                
                if(!array_key_exists($TripleMap->getUri(), $dependency))
                {
                    $dependency[$TripleMap->getUri()] = array();
                }
                $dependency[$TripleMap->getUri()][] = array($parentRef, $ChildMap[0]->getUri());
            }
        }
        $vars = triplemap_vars($TripleMap);
        foreach($vars as $k => $var)
            $tvars[$var] = $TripleMap;
        
    }
    
    //include triplesMap without a logicalSource
    foreach($graph->resourcesMatching(_exp("rr:subjectMap")) as $key => $TripleMap)
    {
        $tmp = array_search($TripleMap, $mapping);
        if(!$tmp)
        {
            $mapping[] = $TripleMap;
        }
    }
    //include triplesMap without a logicalSource
    foreach($graph->resourcesMatching(_exp("rr:subject")) as $key => $TripleMap)
    {
        $tmp = array_search($TripleMap, $mapping);
        if(!$tmp)
        {
            $mapping[] = $TripleMap;
        }
    }
    //triplesMap with only processing chain (use only global variables)
    foreach($graph->resourcesMatching(_exp("rmlx:transform")) as $key => $TripleMap)
    {
        $tmp = array_search($TripleMap, $mapping);
        if(!$tmp)
        {
            $mapping[] = $TripleMap;
            $vars = triplemap_vars($TripleMap);
            foreach($vars as $k => $var)
                $tvars[$var] = $TripleMap;
        }
    }
    //TODO:sort variable dependency
    #foreach($tvars as $k=>$v) echo $k." : ".$v->getUri()."\n";

    foreach($mapping as $k => $tmap)
    {
        $st = triplemap_depvars($tmap);
        foreach($st as $kk => $var)
            if(array_key_exists($var, $tvars) && $tvars[$var] != $tmap)
                $dependency[$tvars[$var]->getUri()][] = array(array(), $tmap->getUri());
    }
    
    $adepth = array();
    foreach($mapping as $k => $map)
    {
        $adepth[$map->getUri()] =_depth($map->getUri(), $dependency);
        //TODO: logical source having a data URI should be after a variable is defined
    }
    #var_dump($dependency);
    arsort($adepth);
    $scount = 0;
    #var_dump($adepth);
    //lookup variables stores values requires for join because of rr:parentTripleMap
    $lookup = array();

    //TODO: one triple map executed more than once (in case variables used in sourceTemplate is an array)
    foreach($adepth as $mapuri => $depth)
    {
        #echo $mapuri, "\n";
        if(array_key_exists($mapuri, $dependency))
        {
            $lookup[$mapuri] = array();
            foreach($dependency[$mapuri] as $kd => $depmap)
            {
                $lookup[$mapuri][$depmap[1]] = array();
            }
        }

        $map = $graph->resource($mapuri);
        process_triplemap($map, $output, $initVars, $lookup, $dependency);
        //if(array_key_exists($mapuri, $lookup)) print_r($lookup[$mapuri]);
    }
    
    $format = EasyRdf_Format::getFormat($oformat);
    $output = $output->serialise($format);
    //time_elapsed();
	#var_dump($initVars);
    //echo $scount;
	return $output;

}

route("/rml/:path", function($arg){
    //header("Content-type: text/json");
    //header("Content-type: text/turtle");
    header("Access-Control-Allow-Origin: *");
	header("Content-type: text/turtle");
    global $mapper;
    //$mapper = array('JSONPath'=>'JsonSource', 'CSV'=>'CSVSource', 'XPath'=>'XmlSource', 'Bibtex'=>'BibtexSource', 'Spreadsheet'=>'SpreadsheetSource');
    $src_path = "tmp/";
    $rml_file = $src_path.$arg["path"].".rml.ttl";
    #echo file_exists($rml_file);
    #echo $arg["path"], $rml_file;
    #print_r(EasyRdf_Format::getFormats());
    
    //echo process_rml($rml_file);
    echo process_rml($rml_file, "turtle", "n3", $initVars=$_GET);
});

route('/test/rmlbibtex', function($arg){
    header("content-type: text/plain");
    $bib = new Structures_BibTex();
    $bib->loadFile("tmp/citations.bib");
    $bib->parse();
    print_r($bib->data);
    $json = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
    $tmp = $json->encode($bib->data);
    $jdata = $json->decode($tmp);
    echo "--";
    //print_r(jsonPath($bib->data, "$.author"));
    print_r(jsonPath($jdata, "$..author[*]"));
});

route('/rml', function($arg){
    if(isset($_GET['rmlsource']))
    {
        header("Access-Control-Allow-Origin: *");
        header("Content-type: text/turtle");
        $content = file_get_contents($_GET['rmlsource']);
        $src_path = "tmp/";
        $rml_file = $src_path.md5($content).".rml.ttl";
        file_put_contents($rml_file, $content);
        $mapper = array('JSONPath'=>'JsonSource', 'CSV'=>'CSVSource', 'XPath'=>'XmlSource', 'Bibtex'=>'BibtexSource', 'Spreadsheet'=>'SpreadsheetSource');
        #echo file_exists($rml_file);
        #echo $arg["path"], $rml_file;
        #print_r(EasyRdf_Format::getFormats());
        //echo process_rml($rml_file);
        echo process_rml($rml_file, "turtle", "n3", $initVars=$_GET);     
    }
    else
	   template('rml', array());
});

route('/rml-proxy', function($arg){
    header('Content-type: text/turtle');
    if(isset($_GET['url']))
    echo file_get_contents($_GET['url']);    
});

route('/exec-rml', function($arg){
	header("Access-Control-Allow-Origin: *");
	$src_path = "tmp/";
    //$content = stripslashes($_POST['data']);
	$content = $_POST['data'];
	$rml_file = $src_path.md5($content).".rml.ttl";
	file_put_contents($rml_file, $content);
	echo process_rml($rml_file, "turtle", "n3", $initVars=$_GET);
}, 'POST');

route('/save-rml', function($arg){
	$src_path = "tmp/";
    //$content = stripslashes($_POST['data']);
	$content = $_POST['data'];
	$rml_file = $src_path.md5($content).".rml.ttl";
	file_put_contents($rml_file, $content);
	$json = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
	echo $json->encode(array('token'=>md5($content)));
}, 'POST');

route('/upload-rml', function($arg){
    $src_path = "tmp/";
    //$content = stripslashes($_POST['data']);
    $content = file_get_contents($_FILES['load-from-file']['tmp_name']);
    $token = md5($content);
    $rml_file = $src_path.$token.".rml.ttl";
    file_put_contents($rml_file, $content);
    $json = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
    //echo $json->encode(array('token'=>md5($content)));
    ?>
    <html>
        <head></head>
        <body>
            <script>
                parent.success_upload("<?php echo $token; ?>")
            </script>
        </body>
    </html>
    <?php
}, 'POST');

route("/rml-source/:path", function($arg){
    //header("Content-type: text/json");
    //header("Content-type: text/turtle");
    header("Access-Control-Allow-Origin: *");
	header("Content-type: text/turtle");
    $src_path = "tmp/";
    $rml_file = $src_path.$arg["path"].".rml.ttl";
	echo file_get_contents($rml_file);
});

route('/save-rdf', function($arg){
    $src_path = "tmp/";
    //$content = stripslashes($_POST['data']);
    $content = $_POST['data'];
    $rml_file = $src_path.md5($content).".ttl";
    file_put_contents($rml_file, $content);
    $json = new Services_JSON(SERVICES_JSON_LOOSE_TYPE);
    echo $json->encode(array('token'=>md5($content)));
}, 'POST');

route("/rdf-source/:path", function($arg){
    //header("Content-type: text/json");
    //header("Content-type: text/turtle");
    header("Access-Control-Allow-Origin: *");
    header("Content-type: text/turtle");
    $src_path = "tmp/";
    $rml_file = $src_path.$arg["path"].".ttl";
    echo file_get_contents($rml_file);
});

route("/rdf-viz",  function($arg){
    template('rdf_graphviz', array());
});

route('/iter-preview', function($args){
    global $mapper;
    $arr = array_combine(array_keys($_GET), array_values($_GET));
    //$arr = json_decode(json_encode($_GET));
    $ql = new EasyRdf_Resource($arr['ref']);
    $arr['localName'] = $ql->localName();
    $src_path = "tmp/";
    //echo json_encode($arr);
    $ref_reader = new $mapper[$ql->localName()]();
    $sourceName = $arr['src'];
    if(substr( $sourceName, 0, 4 ) === "http" || substr( $sourceName, 0, 4 ) === "data")
        $ref_reader->open($sourceName);
    else
        $ref_reader->open($src_path.$sourceName);//local dir
    //$source = json_encode(lookup_source($ref_reader, $_GET['iterator'], array(), array()), JSON_PRETTY_PRINT);
    $source = json_encode($ref_reader->iterate($_GET['iterator']), JSON_PRETTY_PRINT);
    $geshi = new GeSHi($source, 'Javascript');
    echo $geshi->parse_code();
    //echo $ql->localName();
    /*
    $ref_reader = new $mapper[$ql->localName()]();
    $ref_reader->open("data:".$format.",".$data);
    $poolvars[$ovar->getValue()] = lookup_source($ref_reader, $expr, array(), $poolvars);  *
    */
})

?>