<?php

route('/api/', function($arg){
    header('Content-type: application/ld+json');
    $myurl = "http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    $json = new Services_JSON();
    $temp = array(
        "@context"=>array(
              "dc"=>"http://purl.org/dc/elements/1.1/"
            , "dcterms"=>"http://purl.org/dc/terms/"
            , "ckanrdf"=>"http://".$_SERVER["SERVER_NAME"]."/ckanrdf/context/ckanrdf#"
            , "api"=>$myurl
            , "api:submit"=>array("@type"=>"@id")
            )
        , "@type"=>"ckanrdf:EntryPoint"
        , "@id"=>$myurl
        , "api:submit"=>"submit"
        );
    echo $json->encode($temp);
});

route('/context/:fn', function($arg){
    $file = "ctx/".$arg["fn"];
    $myurl = "http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    $data = array("ns"=>$myurl."#", "api"=>"http://".$_SERVER["SERVER_NAME"].root()."/api/", "id"=>$myurl);
    //var_dump($data);
    template($file, $data);
});

route('/context/:fn', function($arg){
    header('Content-type: application/ld+json');
}, "HEAD");