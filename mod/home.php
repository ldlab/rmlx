<?php
route('/',function($arg)
{
    header('Location: rml');
});


route('/widgets/<id:path>',function($arg)
{
    header('Location: ../widget/'.$arg['id']);
});

route('/widget/<id:path>',
function ($arg)
{
	$negotiator = new \Negotiation\FormatNegotiator();
	//$negotiator->registerFormat('jsonld', array('application/ld+json', 'json-ld'));
	header("Cache-Control: no-cache, must-revalidate"); 
	$headers = apache_request_headers();
	$key = "Accept";
	if (!isset($headers[$key])) $key = "ACCEPT";
	$format = $negotiator->getBest($headers[$key], array('text/html','application/ld+json','application/rdf+xml'));
	
    if($format === null)
    {
    	//format requested not applicable
    	http_response_code(406);
    }
    else
    {
    	//handle format by extension first
    	$ext = pathinfo($arg['id'], PATHINFO_EXTENSION);
    	$filename = "__widgets/".$arg['id'];
    	if(file_exists($filename))
    	{
    		header('Content-type: '.ext_to_mime($ext));
    		echo file_get_contents($filename);
    	}
    	else
    	{
    		$filename .= ".".mime_to_ext($format->getValue());
    		if(!file_exists($filename))
    		{
    			require_once('404.php');
    		}
    		else
    		{
    			header('Content-type: '.$format->getValue());
    			echo file_get_contents($filename);
    		}
    	}
    }
});

route('/widgets', function($arg){
	$dirs = get_dir_tree("__widgets", "html");
	//var_dump($dirs);return;
	header("Cache-Control: no-cache, must-revalidate"); 
	header('Content-type: '.mime_to_ext("jsonld"));

	$json = new Services_JSON();
	$myurl = "http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	$temp = array(
		"@context"=>array(
			 "lw"=>"http://linkedwidgets.org/ontology/"
			,"dc"=>"http://purl.org/dc/elements/1.1/"
			,"dcterms"=>"http://purl.org/dc/terms/"
			,"name"=>array("@type"=>"@id", "@id"=>"lw:hasName")
			,"description"=>array("@type"=>"@id", "@id"=>"lw:hasDescription")
			,"hasWidget"=>array("@type"=>"@id", "@id"=>"lw:hasWidget")
			)
		, "@type"=>"lw:WidgetCollection"
		, "@id"=>$myurl
		, "name"=>"Pebbie"
		, "description"=>"Collection of various Widgets"
		);

	foreach($dirs as $k=>$v)
	{

		if(is_array($dirs[$k]))
		{
			foreach($v as $kk=>$vv)
			{
				$ext = pathinfo($kk, PATHINFO_EXTENSION);
				$temp["hasWidget"][] = array("@id"=>$myurl."/".$k."/".substr($kk, 0, -(strlen($ext)+1)));
			}
		}
		else
		{
			$ext = pathinfo($k, PATHINFO_EXTENSION);
			$temp["hasWidget"][] = array("@id"=>$myurl."/".substr($k, 0, -(strlen($ext)+1)));
		}
	}
    echo $json->encode($temp);

	//echo var_export($dirs);
});

route('/services/slugification', function($arg){
    $tmp = $_GET["input"];
    $prefix = isset($_GET["prefix"])?$_GET["prefix"]:"";
    echo $prefix.str_replace(" ", "-", strtolower($tmp));
});

route('/services/genuri', function($arg){
    $prefix = isset($_GET["prefix"])?$_GET["prefix"]:"";
    echo uniqid($prefix);
});

?>