<?php

route('/roman2int', function($args){
	//http://stackoverflow.com/a/6266158
	$romans = array(
	    'M' => 1000,
	    'CM' => 900,
	    'D' => 500,
	    'CD' => 400,
	    'C' => 100,
	    'XC' => 90,
	    'L' => 50,
	    'XL' => 40,
	    'X' => 10,
	    'IX' => 9,
	    'V' => 5,
	    'IV' => 4,
	    'I' => 1,
	);

	if(!isset($_GET['value'])) return;

	$roman = $_GET['value'];//MMMCMXCIX';
	$result = 0;

	foreach ($romans as $key => $value) {
	    while (strpos($roman, $key) === 0) {
	        $result += $value;
	        $roman = substr($roman, strlen($key));
	    }
	}
	echo $result;
});

?>