<?php

function isAssoc($arr)
{
    return array_keys($arr) !== range(0, count($arr) - 1);
}

function str_distance($a,$b)
{
	//asymetric string distance
	//give proportional penalty if $b is not a substring of string $a
	if(strpos($b, $a)===FALSE)
		return 100*strlen($b);
	else
		return levenshtein($a, $b);
}

function dbpedia_lookup($class, $value, $DEBUG=FALSE)
{
	if (strlen($value)==0) return "";
	if($DEBUG) print_r(urlencode($value));
	$xmlresult = file_get_contents("http://lookup.dbpedia.org/api/search/KeywordSearch?QueryClass=".$class."&QueryString=".urlencode($value));
	$xml = simplexml_load_string($xmlresult);
	
	$json = json_encode($xml);
	$xarr = json_decode($json, TRUE);
	if($DEBUG) print_r($xarr);
	if(!isAssoc($xarr)) return "";
	$labels = array();
	if (isAssoc($xarr["Result"]))
	{
		$r = $xarr["Result"];
		$labels[0] = array($r["Label"],$value, $r["URI"]);	
	}
	else
	foreach($xarr["Result"] as $k => $r)
	{
		$labels[$k] = array($r["Label"],$value, $r["URI"]);
	}
	if ($DEBUG) print_r($labels);
	if(count($labels)>0)
	{
		usort($labels, function($a,$b){ if($a[1]==$a[0]) return 0; else return str_distance($a[1], $a[0])-str_distance($b[1], $b[0]);});
		return $labels[0][2];
	}
	return "";
}

route("/rmlx/country-reconcile/<cname:path>", function($arg){
	header("Content-type:text/plain");
	echo dbpedia_lookup("country", $arg["cname"]);
});

route("/rmlx/country-reconcile", function($arg){
	header("Content-type:text/plain");
	usleep(100000);
	echo dbpedia_lookup("country", $_POST["cname"]);
},"POST");

route("/rmlx/org-reconcile/<cname:path>", function($arg){
	header("Content-type:text/plain");
	echo dbpedia_lookup("organisation", $arg["cname"], TRUE);
});

route("/rmlx/org-reconcile", function($arg){
	header("Content-type:text/plain");
	echo dbpedia_lookup("organisation", $_POST["cname"]);
}, "POST");

route("/rmlx/dbpedia-lookup/:class/<cname:path>", function($arg){
	header("Content-type:text/plain");
	echo dbpedia_lookup($arg["class"], $arg["cname"], TRUE);
});

route("/rmlx/dbpedia-lookup", function($arg){
	header("Content-type:text/plain");
	echo dbpedia_lookup($_POST["class"], $_POST["cname"]);
}, "POST");

?>