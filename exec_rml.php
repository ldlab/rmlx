<?php

require_once('root.php');
$param = array();
if(count($argv)>2)
{
	$cnt = 0;
	foreach($argv as $k => $v)
	{
		if($cnt>1 && strpos($v, '='))
		{
			$pair = explode('=', $v);
			$param[$pair[0]] = $pair[1];
		}
		$cnt++;
	}
}
//var_dump($param);
$output = process_rml($argv[1], "turtle", "n3", $initVars=$param);
if(isset($param['output_file']))
	file_put_contents($param['output_file'], $output);
else
	echo '';#$output;

