@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.
@prefix rml: <http://semweb.mmlab.be/ns/rml#>.
@prefix ql: <http://semweb.mmlab.be/ns/ql#>.
@prefix rr: <http://www.w3.org/ns/r2rml#>.
@prefix rmlx: <http://pebbie.org/ns/rmlx#>.
@prefix rmlx-op: <http://pebbie.org/ns/rmlx-functions#>.
@prefix sdmx-dimension: <http://purl.org/linked-data/sdmx/2009/dimension#>.
@prefix sdmx-measure: <http://purl.org/linked-data/sdmx/2009/measure#>.
@prefix sdmx-attribute: <http://purl.org/linked-data/sdmx/2009/attribute#>.
@prefix qb: <http://purl.org/linked-data/cube#>.
@prefix dc: <http://purl.org/dc/terms/>.
@prefix sw: <http://linkedwidgets.org/statisticalwidgets/ontology/>.
@prefix sw-unit: <http://linkedwidgets.org/statisticalwidgets/ontology/unit/>.

<#default>
  rmlx:defaultValue 
    [rmlx:varName "refArea"; rr:constant "http://pebbie.org/ns/wb/countries/GB"],
    [rmlx:varName "subject"; rr:constant "http://data.worldbank.org/indicator/SP.POP.TOTL"],
    [rmlx:varName "unit"; rr:constant "person"];
  rmlx:transform [
        rmlx:outputVar "country_code";
        rmlx:function rmlx-op:replace;
        rmlx:parameterMapping [rmlx:varName "search"; rr:constant "UK"];
        rmlx:parameterMapping [rmlx:varName "replace"; rr:constant "GB"];
        rmlx:parameterMapping [rmlx:varName "subject"; rml:reference "country"]
    ];
  rmlx:transform [
        rmlx:outputVar "subj_part";
        rmlx:function rmlx-op:split;
        rmlx:parameterMapping [rmlx:varName "separator"; rr:constant "/"];
        rmlx:parameterMapping [rmlx:varName "input"; rml:reference "subject"]
    ];
  rmlx:transform [
        rmlx:outputVar "area_part";
        rmlx:function rmlx-op:split;
        rmlx:parameterMapping [rmlx:varName "separator"; rr:constant "/"];
        rmlx:parameterMapping [rmlx:varName "input"; rml:reference "refArea"]
    ];
  rmlx:transform [
        rmlx:outputVar "country";
        rmlx:function rmlx-op:assign;
        rmlx:parameterMapping [rmlx:varName "value"; rml:reference "area_part[-1]"]
    ];
  rmlx:transform [
        rmlx:outputVar "indicator";
        rmlx:function rmlx-op:assign;
        rmlx:parameterMapping [rmlx:varName "value"; rml:reference "subj_part[-1]"]
    ].

<#metamapping>
    rml:logicalSource
            [ rml:iterator
               "$[0]..";
            rml:referenceFormulation
               ql:JSONPath;
            rmlx:sourceTemplate
               "http://api.worldbank.org/countries/{country_code}/indicators/{indicator}?format=json&page=1&per_page=1"];
    rmlx:transform [
        rmlx:outputVar "qparam";
        rmlx:function rmlx-op:assign;
        rmlx:parameterMapping [rmlx:varName "value"; rr:template "?country={country_code}&indicator={indicator}"]
    ].

<#datamapping>
    rml:logicalSource
            [ rml:iterator
               "$[1].*";
            rml:referenceFormulation
               ql:JSONPath;
            rmlx:sourceTemplate
               "http://api.worldbank.org/countries/{country_code}/indicators/{indicator}?format=json&page=1&per_page=15000" ];
    rr:predicateObjectMap
    	[rr:predicate sdmx-dimension:refPeriod; rr:objectMap[ rml:reference "date"; rr:datatype xsd:gYear]];
    rr:predicateObjectMap
    	[rr:predicate sdmx-measure:obsValue; rr:objectMap[ rml:reference "value"; rr:datatype xsd:integer]];
    rr:predicateObjectMap
      [rr:predicate sdmx-dimension:refArea; rr:objectMap[ rml:reference "refArea"; rr:termType rr:IRI]];
    rr:predicateObjectMap
    	[rr:predicate rdfs:label; rr:objectMap[ rr:template "{country.value} population in {date}"]];
    rr:predicateObjectMap
       [ rr:objectMap [rr:template "http://pebbie.org/mashup/rml/wb#{indicator}/dataset"; rr:termType rr:IRI]; rr:predicate qb:dataSet ];
    rr:subjectMap
       [ rr:class qb:Observation; rr:template "http://pebbie.org/mashup/rml/wb#{indicator}/observation/{country.id}_{date}" ].

<#datasetMap>
  rr:subjectMap[ rr:class qb:DataSet; rr:template "http://pebbie.org/mashup/rml/wb#{indicator}/dataset" ];
  rr:predicateObjectMap
       [ rr:objectMap [rr:template "{qparam}"]; rr:predicate rdfs:label ];
  rr:predicateObjectMap
       [ rr:objectMap [rml:reference "subject"; rr:termType rr:IRI]; rr:predicate dc:subject ];
  rr:predicateObjectMap
    [rr:predicate sdmx-attribute:unitMeasure; rr:objectMap [rr:template "http://linkedwidgets.org/statisticalwidgets/ontology/unit/{unit}"; rr:termType rr:IRI]];
  rr:predicateObjectMap
    [rr:predicate qb:structure; rr:objectMap[ rr:termType rr:IRI; rr:template "http://pebbie.org/mashup/rml/wb#{indicator}/dsd" ]].

<#DSDMap>
  rr:subjectMap [ rr:class qb:DataStructureDefinition; rr:template "http://pebbie.org/mashup/rml/wb#{indicator}/dsd" ];
  rr:predicateObjectMap
    [rr:predicate qb:component; rr:objectMap[ rr:termType rr:IRI; rr:template "http://pebbie.org/mashup/rml/wb#{indicator}/dimension/area"]];
  rr:predicateObjectMap
    [rr:predicate qb:component; rr:objectMap[ rr:termType rr:IRI; rr:template "http://pebbie.org/mashup/rml/wb#{indicator}/dimension/period"]];
  rr:predicateObjectMap
    [rr:predicate qb:component; rr:objectMap[ rr:termType rr:IRI; rr:template "http://pebbie.org/mashup/rml/wb#{indicator}/measure/obsVal"]];
  rr:predicateObjectMap
    [rr:predicate qb:component; rr:objectMap[ rr:termType rr:IRI; rr:template "http://pebbie.org/mashup/rml/wb#{indicator}/attribute/unit"]].

<#DimensionMap>
  rr:subjectMap [ rr:class qb:ComponentSpecification; rr:template "http://pebbie.org/mashup/rml/wb#{indicator}/dimension/period" ];
  rr:predicate qb:dimension; 
  rr:object sdmx-dimension:refPeriod.

<#AreaDimensionMap>
  rr:subjectMap [ rr:class qb:ComponentSpecification; rr:template "http://pebbie.org/mashup/rml/wb#{indicator}/dimension/area" ];
  rr:predicate qb:dimension; 
  rr:object sdmx-dimension:refArea.

<#MeasureMap>
  rr:subjectMap [ rr:class qb:ComponentSpecification; rr:template "http://pebbie.org/mashup/rml/wb#{indicator}/measure/obsVal" ];
  rr:predicate qb:measure; 
  rr:object sdmx-measure:obsValue.  

<#AttributeMap>
  rr:subjectMap [ rr:class qb:ComponentSpecification; rr:template "http://pebbie.org/mashup/rml/wb#{indicator}/attribute/unit" ];
  rr:predicateObjectMap
    [rr:predicate qb:attribute; rr:object sdmx-attribute:unitMeasure];
  rr:predicateObjectMap
    [rr:predicate qb:componentAttachment; rr:object qb:DataSet].  