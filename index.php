<?php
//define('DEBUG', true);
$time_start = microtime(1);
session_start();
require_once('root.php');

#header('Content-type: text/plain');
#echo $_GET['q'];
$path = '/' . (isset($_GET['q']) ? $_GET['q'] : '');
$found = false;
$method = $_SERVER['REQUEST_METHOD'];
foreach($route as $pattern => $handler){
    //echo pr($pattern)." ".$path." ".preg_match(pr($pattern), $path, $matches)."\n";
    if(preg_match(pr($pattern), $path, $matches) && (is_callable($handler[$method]) || function_exists($handler[$method]))){
        
        $arg = $matches;
        
        if(isset($routevars[$pattern]) && is_array($routevars[$pattern])){
            $arg = array();
            foreach($routevars[$pattern] as $key=>$idx){
                $arg[$key] = $matches[$idx+1];
            }
        }
        call_user_func($handler[$method], $arg);
        $found = true;
        break;
    }
}
if(!$found)
{
    require_once('404.php');
}
if(defined('DEBUG')){
    function convert($size) {
       $unit=array('b','kb','mb','gb','tb','pb');
       return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
    }
    echo ' '.((microtime(1)-$time_start)*1000).' ms ';
    echo convert(memory_get_peak_usage(true));
}
