<?php


class CurlResponse
{

    var $headers = array();

    function CurlResponse () {}

    function parse_header ($handle, $header)
    {
        #echo $header;
        $details = explode(':', $header, 2);

        if (count($details) == 2)
        {
            $key   = trim($details[0]);
            $value = trim($details[1]);

            $this->headers[$key] = $value;
        }
        else
        {
            $details = explode(' ', $header);
            #var_dump($details);
            if(count($details)==3 && substr($details[0],0,4)=="HTTP")
                $this->headers["HTTP-Code"] = $details[1];  
        }

        return strlen($header);
    }

    function register($curlobj)
    {
        curl_setopt($curlobj, CURLOPT_HEADERFUNCTION, array(&$this, 'parse_header'));
    }

}
/*
$response = new CurlResponse();

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, "http://www.example.org");
curl_setopt($ch, CURLOPT_HEADERFUNCTION, array(&$response, 'parse_header'));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

curl_exec($ch);

print_r($response->headers);
*/
?>