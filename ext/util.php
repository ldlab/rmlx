<?php

use ML\JsonLD\JsonLD;

function is_auth(){
    return session_is_registered('user');
}

function login($user){
    session_register('user');
    $_SESSION['user'] = $user;
}

function logout(){
    session_unset();
    session_destroy();
}

function forcelogin()
{
    if(!is_auth()){
        session_register('auth_redir');
        $_SESSION['auth_redir'] = '/'.$_GET['q'];
        showmessage("Anda harus masuk terlebih dahulu", false);
        redirect('/login');
        return true;
    }
    return false;
}

function check_redir()
{
    $redir = '';
    if(session_is_registered('auth_redir')){
        $redir = $_SESSION['auth_redir'];
        session_unregister('auth_redir');
    }
    redirect($redir);
}

function root(){
    return str_replace('/index.php', '', $_SERVER['SCRIPT_NAME']);
}

function myhost()
{
    return $_SERVER['SERVER_NAME'];
}

function url($url){
    return root().$url;
}

function redirect($url=''){
    header('Location: '.root().$url);
}

function sendmail($targets, $subj, $message, $from=""){
    if ($from=="") $from = "no-reply@".$_SERVER['SERVER_NAME'];
    if(is_array($targets)){
        $tgt = implode(',',$targets);
    }
    else{
        $tgt = $targets;
    }
    db_execute("INSERT INTO mail_log (target, sender, subject, msg) VALUES ('".$tgt."','".$from."','".$subj."','".$message."') ");
    
    if(is_array($targets)){
        foreach($targets as $t){
        mail($t, 
            $subj, 
            $message,
                     "From: ".$from."\r\n" .
                     "Reply-To: ".$from."\r\n" .
                     "X-Mailer: PHP/" . phpversion());
        }
    }
    else{
        mail($targets, 
            $subj, 
            $message,
                     "From: ".$from."\r\n" .
                     "Reply-To: ".$from."\r\n" .
                     "X-Mailer: PHP/" . phpversion());
    }
    
}

function showmessage($msg,$iserror=true){
    session_register('msg');
    $_SESSION['msg'] = $msg;
    session_register('is_msg_error');
    $_SESSION['is_msg_error'] = $iserror;
}

function getmessage(){
    if(session_is_registered('msg')){
        $msg = $_SESSION['msg'];
        session_unregister('msg');
        return $msg;
    }
    return false;
}

function is_msg_error()
{
    if(session_is_registered('is_msg_error')){
        $msg = $_SESSION['is_msg_error'];
        session_unregister('is_msg_error');
        return $msg;
    }
    return true;
}

function gen_random($length)
{
    return substr(md5(rand()), 0, $length);
}

function fromsqltime($sqltime)
{
    $d1 = split(' ', $sqltime);
    $d2 = split('-', $d1[0]);
    $d3 = split(':', $d1[1]);
    return mktime($d3[0], $d3[1], $d3[2], $d2[1], $d2[2], $d2[0]);
}

function tosqltime($time)
{
    return date("Y-m-d H:i:s", $time);
}

function ret_json($tmp)
//requires JSON.php in /ext
{
    $json = new Services_JSON();
    header('Content-type: application/json');
    echo $json->encode($tmp);
}

function load_json($url)
{
    $json = new Services_JSON();
    $content = file_get_contents($url);
    return $json->decode($content);
}

function ret_jsonld($jsonld)
{
    header('Content-type: application/ld+json');
    echo $jsonld;
    JsonLD::toString($jsonld);
}

function filter_inst($var)
{
    return (strncmp($var, "install_", 8)==0);
}

function add_log($message)
{
    file_put_contents("log.txt", $message."\n", FILE_APPEND);
}

function handle_upload($key, $folder, $prefix='')
{
    if($_FILES[$key]['name']!=''){
        $dir = 'static/'.$folder;
        if (!file_exists($dir)) mkdir($dir);
        
        $filename = $_FILES[$key]['name'];
        $fileext = substr($filename, strrpos($filename, '.'));
        $tmpFile = $_FILES[$key]['tmp_name'];
        $uploadFile = 'static/'.$folder.'/'.$prefix.$fileext;
        $uploaded = move_uploaded_file($tmpFile, $uploadFile);
        return $uploadFile;
    }
}

function install($arg)
{
    $sql = "
    CREATE TABLE IF NOT EXISTS `mail_log` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
      `target` tinytext NOT NULL,
      `sender` text NOT NULL,
      `subject` tinytext NOT NULL,
      `msg` text NOT NULL,
      PRIMARY KEY (`id`),
      FULLTEXT KEY `msg` (`msg`)
    ) ENGINE=MyISAM  DEFAULT CHARSET=latin1 ;
    ";
    db_open($_POST['my_dbhost'], $_POST['my_dbuser'], $_POST['my_dbpass'], $_POST['my_dbname']);
    add_log("creating boot.php");
    file_put_contents("boot.php", "<?php
        
db_open('".$_POST['my_dbhost']."', '".$_POST['my_dbuser']."', '".$_POST['my_dbpass']."', '".$_POST['my_dbname']."');
        
    ");
    
    add_log("installing database");
    $ret = db_execute($sql);
    add_log($ret);
    $fns = get_defined_functions();
    $ufn = array_filter($fns['user'], "filter_inst");
    foreach($ufn as $f){
        add_log("executing ".$f);
        call_user_func($f);
    }
    add_log("installation done!");
    //echo str_replace("\n","<br/>", file_get_contents("log.txt"));
    redirect('/');
}

function ext_to_mime($ext)
{
    switch($ext)
    {
        case 'html': return 'text/html';break;
        case 'rdf': return 'application/rdf+xml';break;
        case 'json': return 'application/json';break;
        case 'jsonld': return 'application/ld+json';break;
        case 'txt': return 'text/plain';break;
        case 'n3': return 'text/n3'; break;
        case 'ttl': return 'text/turtle'; break;
        case 'nt': return 'application/n-triples'; break;
        case 'nq': return 'application/n-quads'; break;
    }
}

function mime_to_ext($mime)
{
    switch($mime)
    {
        case 'application/rdf+xml': return 'rdf'; break;
        case 'application/ld+json': return 'jsonld'; break;
        case 'application/json': return 'json'; break;
        case 'text/plain': return 'txt'; break;
        case 'text/html': return 'html'; break;
        case 'text/n3': return 'n3'; break;
        case 'text/turtle': return 'ttl'; break;
        case 'application/n-quads': return 'nq'; break;
    }
}

function get_dir_tree( $dir , $ext){ 
    //http://php.net/manual/en/function.scandir.php
    $dirs = array_diff( scandir( $dir ), Array( ".", ".." ) ); 
    //echo var_export($dirs);
    $dir_array = Array(); 
    foreach( $dirs as $d ){ 
        if( is_dir($dir."/".$d)  ){ 
            $dir_array[ $d ] = get_dir_tree( $dir."/".$d , $ext); 
            //$dir_array[] = get_dir_tree( $dir."/".$d , $ext); 
        }else{ 
            //if(preg_match("/.*".$ext.'$/s',$d)===FALSE)echo "ERR";
            //echo strlen($ext)." -".preg_match("/.*".$ext.'$/s',$d)."-<br/>";
         if (strlen($ext)==0 || preg_match("/.*".$ext.'$/s',$d)) 
            $dir_array[ $d ] = $d; 
            //$dir_array[] = $d; 
            } 
    } 
    return $dir_array; 
}