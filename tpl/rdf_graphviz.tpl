<html>
	<head>
		<title>RDF GraphViz</title>
		<style type="text/css">
    body {
      margin: 0;
    }
    #container {
      position: absolute;
      width: 100%;
      height: 100%;
    }
  </style>
	</head>
	<body>
		<div id="container"></div>
		<script src="js/jquery-2.1.4.min.js"></script>
		<script src="js/sigma.min.js"></script>
		<script src="js/plugins/sigma.layout.forceAtlas2.min.js"></script>
		<script src="js/plugins/sigma.renderers.edgeLabels.min.js"></script>
		<script src="js/plugins/sigma.renderers.customShapes.min.js"></script>
		<script src="js/rdflib.js"></script>
		<!--<script src="js/rdflib-rdfa.js"></script>-->
		<script>
			 function parseUrl(url)
	        {
	            var parser = document.createElement('a');
	            parser.href = url;
	            return {protocol:parser.protocol, domain:parser.hostname, port:parser.port, path:parser.pathname, hash:parser.hash, server:parser.host, param:parser.search}
	        }

	        function qname(uri, kb)
	        {
	        	var tmp = uri;
	        	$.each(kb.namespaces, function(nsprefix,nsuri){ 
	        		if(uri.startsWith(nsuri)){
	        			console.log(nsprefix, uri, uri.replace(nsuri, nsprefix+':'))
	        			tmp = uri.replace(nsuri, nsprefix+':')
	        		}
	        	})
	        	return tmp
	        }

	        var myloc = parseUrl(window.location.href)
	        console.log(myloc)

			var RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#")
			var RDFS = $rdf.Namespace("http://www.w3.org/2000/01/rdf-schema#")
			var OWL = $rdf.Namespace("http://www.w3.org/2002/07/owl#")
			var OL = $rdf.Namespace("http://www.w3.org/2007/ont/")
			var DCT = $rdf.Namespace("http://purl.org/dc/terms/")
			
			var rdfs_prefix = RDFS().uri
			var owl_prefix = OWL().uri

			var kb = new $rdf.graph()
			fetcher = $rdf.fetcher(kb)

			var s = new sigma({
			  renderer: {
			    container: document.getElementById('container'),
			    type: 'canvas'
			  },
			  settings: {
			    edgeLabelSize: 'proportional',
			    minArrowSize: 10
			  }
			})
			CustomShapes.init(s)
			s.refresh()
			var vg = s.graph
			var path = []

			docURI = 'lw.ttl'
			if(myloc.hash.length>0) docURI = myloc.hash.substr(1)
			docUrl = parseUrl(docURI)

			//fix relative path
			docURI = docUrl.protocol+"//"+docUrl.server+docUrl.path+docUrl.param+docUrl.hash;
			
			retUrl = parseUrl("retrieve.php")
			if(docUrl.domain != myloc.domain)
				docURI = retUrl.protocol+"//"+retUrl.server+retUrl.path+"?uri="+docURI

			console.log(docURI)
			
			//global counter for edge id
			var gctr = 0

			function randloc()
			{
				y = Math.round(Math.random()*$("#container").height())
				x = Math.round(Math.random()*$("#container").width())
				return {x:x,y:y}
			}

			function draw_focus(obj)
			{
				po = path.indexOf(obj)
				if(po>=0 && po==path.length-2)
				{
					//return to previous navigation
					path.pop()
				}
				else path.push(obj)
				var lastnode = undefined
				if(path.length>1)
					lastnode = path[path.length-2]
				vg.clear()

				if(vg.nodes(obj.value)==undefined)
				{
					//l = randloc()
					vg.addNode({id:obj.value, label:obj.value, color:"green", size:1, x:0, y:0, ref:obj})
				}
				if(lastnode != undefined)
				{
					vg.addNode({id:lastnode.value, label:lastnode.value, color:"blue", size:1, x:-100, y:-30, ref:lastnode})
					vg.addEdge({id:gctr++, source:lastnode.value, target:obj.value, color:"silver", size:1, type:"curve"})
				}

				dir = 1
				ddist = 24
				dist = 24
				$.each(kb.each(obj), function(_,pred){
					$.each(kb.each(obj, pred), function(_, oo){
						//if(oo.termType=="symbol")
						{
							var yloc = dir * dist;
							var cid

							if(oo instanceof $rdf.Literal)//could be multiple
							{
								cid = 'o'+gctr++
								vg.addNode({id:cid, label:oo.value.substr(0, 40)+'...', size:1, x:250, y:yloc, ref:oo})	
							}
							else if(vg.nodes(oo.value)==undefined)
							{
								cid = oo.value
								vg.addNode({id:cid, label:oo.value, size:1, x:250, y:yloc, ref:oo})
							} 
							else return

							vg.addEdge({id:pred.value+gctr++, label:qname(pred.uri, kb), source:obj.value, target:cid, color:"silver", size:1, type:"line"})
							dir *= -1
							if(dir>0)
							{
								dist += ddist
							}
						}
					})
				})
				s.refresh()
				//s.startForceAtlas2({})
				//setTimeout(function(){s.killForceAtlas2()}, 600)
			}

			function get_label(node, kb)
			{
				var lbl = kb.the(node, RDFS('label'))
				if(lbl!=undefined)
					lbl_val = lbl.value
				else
					lbl_val = qname(node.uri, kb)
				return lbl_val
			}

			function draw_tbox()
			{
				path = []
				vg.clear()
				classes = {}
				skipnodes = []

				var cls = kb.each(undefined, RDF('type'), OWL('Class'))
				var ops = kb.each(undefined, RDF('type'), OWL('ObjectProperty'))
				var dps = kb.each(undefined, RDF('type'), OWL('DatatypeProperty'))
				var scs = kb.each(undefined, RDFS('subClassOf'))
				var hps = kb.each(undefined, DCT('hasPart'))

				ops = ops.concat(dps)

				for(var i=0; i<ops.length; ++i)
				{
					pred = ops[i]
					console.log(pred.value)

					l=randloc()
					vg.addNode({id:pred.uri, label:get_label(pred, kb), size:1, x:l.x, y:l.y, ref:pred})
					//domain
					dom_obj = kb.the(pred, RDFS('domain'))
					if(dom_obj != undefined && dom_obj.termType=='symbol')
					{
						//console.log(dom_obj.termType, dom_obj)
						if(vg.nodes(dom_obj.uri)==undefined)
						{
							l=randloc()
							vg.addNode({id:dom_obj.uri, label:get_label(dom_obj, kb), size:1, x:l.x, y:l.y, type:'square', ref:dom_obj})	
						}
						vg.addEdge({id:'a'+gctr++, source:pred.uri, target:dom_obj.uri, color:"green", size:1, type:"straight"})
					}

					rng_obj = kb.the(pred, RDFS('range'))
					if(rng_obj != undefined && rng_obj.termType=='symbol')
					{
						if(vg.nodes(rng_obj.uri)==undefined)
						{
							l=randloc()
							vg.addNode({id:rng_obj.uri, label:get_label(rng_obj, kb), size:1, x:l.x, y:l.y, type:'square', ref:rng_obj})	
						}
						vg.addEdge({id:'a'+gctr++, source:pred.uri, target:rng_obj.uri, color:"orange", size:1, type:"straight"})
					}
				}

				//rdfs:subClassOf
				for(var i=0; i<scs.length; ++i)
				{
					sub_obj = scs[i]
					sup_obj = kb.the(sub_obj, RDFS('subClassOf'))

					//domain
					if(sup_obj != undefined && sup_obj.termType=='symbol')
					{
						if(vg.nodes(sub_obj.uri)==undefined)
						{
							l=randloc()
							vg.addNode({id:sub_obj.uri, label:get_label(sub_obj, kb), size:1, x:l.x, y:l.y, type:'square', ref:sub_obj})	
						}

						//console.log(dom_obj.termType, dom_obj)
						if(vg.nodes(sup_obj.uri)==undefined)
						{
							l=randloc()
							vg.addNode({id:sup_obj.uri, label:get_label(sup_obj, kb), size:1, x:l.x, y:l.y, type:'square', ref:sup_obj})	
						}
						vg.addEdge({id:'a'+gctr++, source:sub_obj.uri, target:sup_obj.uri, color:"blue", size:1, type:"arrow", arrow:"target"})
					}

				}
				s.refresh()
				//s.startForceAtlas2({})
			}

			function draw_abox()
			{
				path = []
				vg.clear()
				classes = {}
				skipnodes = []

				var indivs = kb.each(undefined, RDF('type'))
				for(i=0; i<indivs.length; i++)
				{
					indiv = indivs[i]

					cls = kb.the(indiv, RDF('type'))
					if(cls.uri.startsWith(owl_prefix) || (cls.uri.startsWith(rdfs_prefix)) || (cls.uri.startsWith(OL().uri)) )
					{
						console.log(cls.uri)
						skipnodes.push(indiv)
						continue
					}

					//force Sym to string
					iid = indiv
					if(classes[cls.uri]==undefined)
						classes[cls.uri] = [iid]
					else
						if(classes[cls.uri].indexOf(iid)==-1)
							classes[cls.uri].push(iid)
				}

				//case when the assertions has no rdf:type links
				if(Object.keys(classes).length==0)
				{
					cls = RDF('resource')
					classes[cls.uri] = []
					tnodes = []
					
					function skipchildnodes(inode, blacklist, buffer)
					{
						var childs = []
						var iprops = kb.each(inode)
						for(var p=0;p<iprops.length;p++)
						{
							$.each(kb.each(inode, iprops[p]), function(k, nobj){

							if(!(nobj instanceof $rdf.Literal))
							{
								blacklist.push(nobj)
								while(buffer.indexOf(nobj)!=-1)
								{
									console.log("remove", nobj)
									buffer.splice(buffer.indexOf(nobj),1)
								}
								if(buffer.length>0)
									console.log("isi buffer", buffer)
								childs.push(nobj)
							}
							
							})
						}
						for(var p=0;p<childs.length;p++)
						{
							skipchildnodes(childs[p], blacklist, buffer)
						}
					}

					allnodes = kb.each()
					filtnodes = []
					$.each(allnodes, function(k, node){
						if(filtnodes.indexOf(node)==-1)
							filtnodes.push(node)
					})
					$.each(filtnodes, function(k, node){
						//console.log('pass 1', node.value)
						props = kb.each(node)
						for(p=0;p<props.length;p++)
						{
							//console.log(node.value, props[p].uri, props[p].uri.startsWith(OL().uri))
							if(props[p].uri.startsWith(OL().uri))
							{
								skipnodes.push(node)
								while(tnodes.indexOf(node) !=-1)
									tnodes.splice(tnodes.indexOf(node),1) 
								break;
							}
						}
						if(skipnodes.indexOf(node)==-1)
						{
							console.log("skipping", node)
							skipchildnodes(node, skipnodes, tnodes)
							if(tnodes.indexOf(node)==-1)
							{
								console.log("add", node)
								tnodes.push(node)
							}
						}
					})
					$.each(tnodes, function(k, node){
						console.log("skip 2 ", node, skipnodes.indexOf(node), tnodes.indexOf(node))
						skipchildnodes(node, skipnodes, tnodes)
					})
					console.log('tnodes', tnodes)
					$.each(tnodes, function(k, node){
						props = kb.each(node)
						//console.log(node.value, kb.each(node), kb.any(undefined, undefined, node))
						if(skipnodes.indexOf(node)==-1)
							classes[cls.uri].push(node)
					})
				}
				//console.log(classes)

				$.each(classes, function(cls, objs){
					//prune OWL (Classes, Properties) & RDFS (Literals)

					//console.log(cls)
					//console.log(objs.length)

					//avoid dup
					if(vg.nodes(cls)==undefined)
					{
						l=randloc()
						vg.addNode({id:cls, label:cls, size:objs.length, x:l.x, y:l.y, color:'green', ref:kb.sym(cls)})
					}

					$.each(objs, function(oid, obj){
						//avoid dup
						if(obj.termType!='symbol')
							console.log(obj)
						if(vg.nodes(obj.value)==undefined)
						{
							l = randloc()
							vg.addNode({id:obj.value, label:obj.value, size:kb.each(obj).length, x:l.x, y:l.y, ref:obj})
						}

						//connect individual to its class
						vg.addEdge({id:'a'+gctr++, source:obj.value, target:cls, color:"silver", size:1, type:"curve"})

					})
				})

				//layout the graph, stop after some time (no autostop otherwise)
				s.startForceAtlas2({})
				//setTimeout(function(){s.killForceAtlas2()}, 600)
			}

			function is_ontology()
			{
				return kb.the(undefined, RDF('type'), OWL('Ontology')) != undefined
			}

			function draw_overview()
			{
				if(is_ontology())
				{
					console.log('it is a vocabulary')
					draw_tbox()
				}
				else
					draw_abox()	
			}

			fetcher.nowOrWhenFetched(docURI, docURI, function(ok, data, xhr){	
				//$.each(kb.the(kb.the(undefined, OL('link#status')), OL('link#status')).elements, function(i, e){console.log(e.value)})
				if(ok==true)
				{
					//console.log(kb.serialize(docURI, 'text/n3'))	
					draw_overview()					
				}
				else console.log(data)

			})

			s.bind('clickNode', function(e) {
				console.log(e.type, e.data.node.ref);
				draw_focus(e.data.node.ref)
			});

			s.bind('overEdge outEdge clickEdge doubleClickEdge rightClickEdge', function(e) {
				console.log(e.type, e.data.edge, e.data.captor);
			});

			s.bind('doubleClickStage', function(e) {
			  	draw_overview();
			});

			s.bind('rightClickStage', function(e) {
			  	if(s.isForceAtlas2Running())
			  		s.killForceAtlas2()
			});


			function setup_dnd(droparea)
			{
				var api_available = false
				if (typeof window.FileReader === 'undefined') {
				} else {
				  api_available = true
				}
 
				holder.ondragover = function () { this.className = 'hover'; return false; };
				holder.ondragend = function () { this.className = ''; return false; };
				holder.ondrop = function (e) {
				  this.className = '';
				  e.preventDefault();

				  var file = e.dataTransfer.files[0],
				      reader = new FileReader();
				  reader.onload = function (event) {
				    console.log(event.target);
				    holder.style.background = 'url(' + event.target.result + ') no-repeat center';
				  };
				  console.log(file);
				  reader.readAsDataURL(file);

				  return false;
				};
			}
		</script>
	</body>
</html>