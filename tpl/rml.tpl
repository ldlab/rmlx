<!DOCTYPE html>
<html>
	<head>
		<title>RMLx Visual Editor</title>
        <link rel="stylesheet" href="css/materialize.min.css">
		<!--<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">-->
		<link href="css/materialize-icons.css" rel="stylesheet">
        <style type="text/css">
        /*https://jsfiddle.net/ypcrumble/ht379nq5/*/
    .autocomplete-content {
      position: absolute;
      background: #383838;
      margin-top: -.9rem;
      z-index: 9999;
    }
    
    .autocomplete-content li {
      clear: both;
      color: rgba(0, 0, 0, 0.87);
      cursor: pointer;
      line-height: 0;
      width: 100%;
      text-align: left;
      text-transform: none;
      padding: 10px;
    }
    
    .autocomplete-content li > span {
      color: #ffa726;
      font-size: .9rem;
      padding: 1.2rem;
      display: block;
    }
    
    .autocomplete-content li > span .highlight {
      color: #000000;
    }
    
    .autocomplete-content li img {
      height: 52px;
      width: 52px;
      padding: 5px;
      margin: 0 15px;
    }
    
    .autocomplete-content li:hover {
      background: #eee;
      cursor: pointer;
    }
    
    .autocomplete-content > li:hover {
      background: #292929;
    }

        </style>
	</head>
	<body>
		<ul class="dropdown-content" id="rml-project">
			<li><a href="#!" onclick="clear_rml()">New</a></li>
			<li><a href="#!" onclick="load_rml()">Load</a></li>
			<li><a href="#!" onclick="download_rml()">Save</a></li>
		</ul>
		<ul class="dropdown-content" id="rml-tools">
			<li><a href="#!"  onclick="rdf_viz_src()">Visualize</a></li>
		</ul>
		<ul class="dropdown-content" id="rml-extras">
			<li><a href="#!"  onclick="save_lw()">Export as Linked Widget</a></li>
			<li><a href="#!"  onclick="show_about()">About</a></li>
		</ul>
		<nav>
			<div class="nav-wrapper">
				<a href="#" class="brand-logo">&nbsp;RMLx Visual Editor</a>
			  <ul id="nav-mobile" class="right hide-on-med-and-down">
			  	<li><a class="dropdown-button col s4" href="#!" data-activates="rml-project">RMLx Project<i class="material-icons right">arrow_drop_down</i></a></li>
			    <li><a class="dropdown-button" href="#!" data-activates="rml-tools">Tools<i class="material-icons right">arrow_drop_down</i></a></li>
			    <li><a class="dropdown-button" href="#!" data-activates="rml-extras">Extras<i class="material-icons right">arrow_drop_down</i></a></li>
			  </ul>
		  </div>
		</nav>

		<div class="container" id='main-container'>
			
			<br/>
			<div class="row">
				<div class="col s12">
					
				</div>
			</div>
			<br />
			
			<div class="row">

			<div class="col s12">
				<ul id="tabs" class="tabs" style="overflow:hidden;">
				  <li class="tab col"><a href="#vocab-panel">Vocabulary</a></li>
				  <li class="tab col"><a href="#mapping-panel"  class="active">Mapping</a></li>
				  <li class="tab col"><a href="#output-panel">Output</a></li>
				  <!--<li class="tab col"><a href="#help-panel">Help</a></li>-->
				</ul>
			</div>
			
			<br />

			<div id="vocab-panel" class="col s12">
				<div class="row">
					<div class="col s12" id="vocablist">
						<table class="table table-condensed">
							<thead>
							<tr>
								<th class="col s2">prefix</th><th colspan="2">URI</th>
							</tr>
							</thead>
							<tbody id="vlist">
							</tbody>
						</table>
					</div>
					<div class="col s12">
						<div class="input-group">
							<input type="text" id="addvocab" placeholder='Import vocabulary (prefix/URI) or use "prefix=URI" to introduce new prefix (e.g.: qb=http://purl.org/linked-data/cube# )'/>
							<span class="input-group-btn"><button class="btn btn-default" onclick="import_vocab()">Import</button></span>
						</div>
					</div>
				</div>
				<div class="row hide" id="vocab-content">
					<div class="col s4" id="classes-pane">
						<h5>Classes</h5>
						<ul id="classes"></ul>
					</div>
					<div class="col s4" id="properties-pane">
						<h5>Properties</h5>
						<ul id="properties"></ul>
					</div>
				</div>
			</div>

			<div id="mapping-panel" class="col s12">

				<br/>
				<div class="row">
					<div class="fixed-action-btn">
						<a href="#" class="btn-floating btn-large red"><i class="material-icons">add</i></a>
						<ul>
						<a onclick="add_mapping()" class="btn-floating pink darken-4">map</a>
						<a onclick="add_mapping_csv()" class="btn-floating">csv</a>
						<a onclick="add_mapping_xml()" class="btn-floating">xml</a>
						<a onclick="add_mapping_json()" class="btn-floating">json</a>
						</ul>
					</div>
				</div>
				<div class="col-md-12"></div>
				
			<div class="row" id="mappingbody">
			<!--<div class="row card-list">
				<ul class="list-inline" id="mappingbody">
				<li class="card">-->
				<div class="card tripleMapping col s4" id="mapping-{{id}}">
					<!--<div class="header"><span class="close" onclick="$(this.parentElement.parentElement.parentElement).remove()">&times;</span></div>-->
					<div class="card-content">
						<span class="card-title"><span id="mapping-title-{{id}}">Triple Mapping {{id}}</span></span>
							
							<a class="btn-floating btn-small red right" onclick="$(this.parentElement.parentElement).remove()"><i class="material-icons">delete</i></a>	
						
						<ul class="col s12 collapsible" data-collapsible="accordion" id="collapsible-{{id}}">
							<li>
								<div class="collapsible-header"><span id="source-{{id}}">CSV</span> Source</div>
								<div class="collapsible-body">

							<div class="input-field label-info" style="display:none;">
								<input type="hidden" id="maptitle-{{id}}" value="#Untitled{{id}}Mapping"/>
								<label for="maptitle-{{id}}" class="active">Mapping Name</label>
							</div>
							<div class="input-field">
								<select name="mapSrcType-{{id}}" id="mapSrcType-{{id}}" onchange="update_sourcetype(this)">
									<option value="URI">plain URI</option>
									<option value="http://www.w3.org/2011/http#requestURI">URI Template</option>
									<option value="no-source">No Source</option>
									<option value="http://www.w3.org/ns/dcat#Dataset" disabled>DCAT</option>
									<option value="http://www.w3.org/ns/hydra/core#IriTemplate" disabled>HYDRA</option>
									<option value="http://www.w3.org/ns/sparql-service-description#Service" disabled>SPARQL</option>
									<option value="http://linkedwidgets.org/ontology/Input" disabled>Widget Terminal</option>
									<option value="http://linkedwidgets.org/ontology/Option" disabled>Widget Option</option>
								</select>
								<label for="mapSrcType-{{id}}">Source Type</label>
							</div>
							<div class="input-field">
								<input type="text" id="mapSource-{{id}}" placeholder="http://"/>
								<label for="mapSource-{{id}}" class="active">Logical Source</label>
							</div>
							<div class="input-field">
								<select id="mapQL-{{id}}" onchange="update_ql(this)">
									<option value="http://semweb.mmlab.be/ns/ql#CSV" data-content="CSV">CSV</option>
									<option value="http://semweb.mmlab.be/ns/ql#JSONPath" data-content="JSON">JSONPath (json)</option>
									<option value="http://semweb.mmlab.be/ns/ql#XPath" data-content="XML">XPath (xml)</option>
									<option value="http://pebbie.org/ns/ql#HTMLXPath" data-content="HTML">XPath (html)</option>
									<option value="http://pebbie.org/ns/ql#Spreadsheet" data-content="XLS(X)">Spreadsheet (csv,xls,xlsx)</option>
									<option value="http://pebbie.org/ns/ql#Pivot" data-content="XL Pivot">Pivot Table (csv,xls,xlsx)</option>
									<option value="http://pebbie.org/ns/ql#CSS" data-content="HTML">CSS (html)</option>
									<option value="http://pebbie.org/ns/ql#RegExp"  data-content="Text">RegExp (text)</option>
								</select>
								<label for="mapQL-{{id}}">Query Language</label>
							</div>
							<div class="row">
								<div class="input-field col s10">
									<input type="text" id="mapIterator-{{id}}" />
									<label for="mapIterator-{{id}}">Iterator</label>

									<!--<div class="input-group-btn">
										<button type="button" class="btn btn-default" id="seliterator-{{id}}" onclick="select_iterator(this);" data-target="modal" disabled>preview</button>
									</div>-->
								</div>
								<div style="padding-top:1.5em;">
								<a class="btn-floating btn-small tooltipped" id="seliterator-{{id}}" onclick="select_iterator(this);" data-position="top" data-tooltip="preview iterator"><i class="material-icons">search</i>preview</a>
								</div>
								
							</div>

							</div>
							</li>
						</ul>


							<div class="row">
								<!--TODO: the position of checkbox is too low-->
								<div class="col s12">
								<p>
									<input type="checkbox" id="mapSubjIsBNode-{{id}}" onchange="update_subjtempl(this)"/>
									<label for="mapSubjIsBNode-{{id}}">Blank Node</label>
								</p></div>
								<div class="col s12 input-field">
									<input type="text" id="mapSubj-{{id}}" placeholder="http://ex.org/{id}" />
									<label for="mapSubj-{{id}}" class="active">Subject Template</label>
								</div>
							</div>
							<div class="input-field">
								<input type="text" id="mapCls-{{id}}" class="autocomplete" data-termtype="class" onkeyup="test_autocomplete(this)" onchange="update_map_title(this)"/>
								<label for="mapCls-{{id}}">Is A</label>
							</div>
						</div>

					<!--placeholder for transform block-->
					<div class="xform">
						<div class="row" id="operator">
							<div class="card-content">
								<span class="card-title activator"><span id="xform-title-{{id}}" class="activator">Transform</span><i class="material-icons right">mode_edit</i></span>
							</div>
							<div class="card-reveal">
								<span class="card-title ">Transform<i class="material-icons right">close</i></span>
								<div class="col s12 input-field">
									<input type="text" id="fn-ovar-{{id}}" />
									<label for="fn-ovar-{{id}}" class="active">Output Var</label>
									</div>
								<div class="col s12 input-field">
									<select name="fn-{{id}}" id="fn-{{id}}" onchange="update_fn(this)">
										<option value="split" data-param="separator,input">Split string</option>
										<option value="replace" data-param="search,replace,subject">Replace string</option>
										<option value="trim" data-param="input">Trim string</option>
										<option value="uppercase" data-param="input">Uppercase</option>
										<option value="lowercase" data-param="input">Lowercase</option>
										<option value="assign" data-param="value">Assign Value</option>
										<option value="http" data-param="">REST API</option>
									</select>
									<label for="fn-{{id}}">Function</label>
									</div>
								<div id="fn-httpopt-{{id}}" style="display:none;">
									<div class="col s7 input-field">
										<input type="text" id="fn-httpuri-{{id}}"/>
										<label for="fn-httpuri-{{id}}">Request URI</label>
										</div>
									<div class="col s5 input-field">
										<select name="fn-httpmethod-{{id}}" id="fn-httpmethod-{{id}}" >
											<option value="GET">GET</option>
											<option value="POST">POST</option>
											<option value="PUT">PUT</option>
											<option value="DELETE">DELETE</option>
										</select>
										<label for="fn-httpmethod-{{id}}">HTTP Method</label>
									</div>
									<div class="input-field col s12">
										<button id="update_fn_btn-{{id}}" class="btn btn-small" onclick="update_fn_http(this)">update parameters</button>
										</div>
									</div>
								<div class="col s12 params-list">
									<!-- <div class="param-group" id="param-tpl">-->
										<div class="col s7 input-field">
											<input type="text" disabled id="param-{{ctr}}-{{id}}" value="{{id}}"/>
											<label for="param-{{ctr}}-{{id}}" class="active">Param</label>
											</div>
										<div class="col s5 input-field">
											<select name="pvmap-{{ctr}}-{{id}}" id="pvmap-{{ctr}}-{{id}}">
												<option value="constant">Constant</option>
												<option value="reference">Reference</option>
												<option value="template">Template</option>
											</select>
											<label for="pvmap-{{ctr}}-{{id}}">Type</label>
											</div>
										<div class="col s12 cref">
											<input type="text" id="paramVal-{{ctr}}-{{id}}"/>
											<label for="paramVal-{{ctr}}-{{id}}" class="objval active">Value</label>
											</div>
									<!--</div>-->
									</div>
							</div>
							<div class="card-action">
								<a href="#" onclick="remove_po(this)"><i class="material-icons right" style="color:#e65100">delete</i></a>
							</div>
						</div>
					</div>

					<!--placeholder for predicate-object map block-->
					<div class="pomap">
						<div class="card-content">
							<span class="card-title activator"><span id="pomap-title-{{id}}" class="activator">PO Map</span><i class="material-icons right">mode_edit</i></span>
						</div>
						<div class="card-reveal">
							<span class="card-title grey-text text-darken-4">PO Map<i class="material-icons right">close</i></span>
							<div class="row" id="pomap-body-{{id}}">
								<!--TODO: support predicate map -->
								<div class="col s12 input-field">
									<select name="omap-{{id}}" id="omap-{{id}}" onchange="update_omap(this)">
										<option value="constant">Constant</option>
										<option value="reference">Reference</option>
										<option value="template">Template</option>
										<option value="join">Join</option>
									</select>
									<label for="omap-{{id}}">Value Type</label>
								</div>
								<div class="col s12 input-field">
									<input type="text" id="pomap-pred-{{id}}" onchange="update_card_title(this)" class="autocomplete" data-termtype="property" onkeyup="test_autocomplete(this)"/>
									<label for="pomap-pred-{{id}}" class="active">Predicate</label>
									</div>
								<div class="col s12 row">
									<div class="col s4 input-field" id="mapObjttypec-{{id}}">
										<input type="checkbox" id="mapObjttype-{{id}}">
										<label for="mapObjttype-{{id}}">IRI</label>
									</div>
									<div class="col s8 input-field" id="mapObjdtypec-{{id}}">
										<input type="checkbox" id="mapObjdtype-{{id}}" onchange="update_dtype(this)" onkeyup="test_autocomplete(this)">
										<label for="mapObjdtype-{{id}}">use Data type</label>
									</div>
								</div>
								<div class="col s12 cref input-field" >
									<input type="text" id="mapObj-{{id}}" />
									<label for="mapObj-{{id}}" class="objval active">Value</label>
								</div>
								<div class="col s12 input-field" style="display:none;" id="mapObjdtc-{{id}}">
									<input type="text" id="mapObjdt-{{id}}"  />
									<label for="mapObjdt-{{id}}" class="objval">Data type</label>
								</div>
								<div class="col s12 input-field" id="mapObjlanguagec-{{id}}">
									<input type="checkbox" id="mapObjlanguage-{{id}}" onchange="update_language(this);">
									<label for="mapObjlanguage-{{id}}">use language ID</label>
								</div>
								<div class="col s12 input-field" style="display:none;" id="mapObjlangc-{{id}}">
									<input type="text" id="mapObjlang-{{id}}"  />
									<label for="mapObjlang-{{id}}" class="objval" placeholder="en">Language ID</label>
								</div>
								<div class="col s12 joinval" style="display:none;">
									<div class="row">
										<div class="input-field col s12">
										<input type="text" id="parent-{{id}}" />
										<label for="parent-{{id}}" class="active">Parent Value</label>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s12">
										<input type="text" id="child-{{id}}" />
										<label for="child-{{id}}" class="active">Child Value</label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card-action">
							<a href="#" onclick="remove_po(this)"><i class="material-icons right" style="color:#b71c1c;">delete</i></a>
						</div>
					</div>


					<div class="card-action">Add&nbsp;
						<a href="#" id="add-pomap-{{id}}" onclick="add_pomap(this)">Pred-Obj</a>
						<a href="#" id="add-xform-{{id}}" onclick="add_xform(this)">transform</a>
					</div>
				</div>
				
			</div>
			</div>

			<div class="modal modal-fixed-footer" id="preview-modal">
				<div class="modal-content">
				<form id="preview-form" action="iter-preview" target="preview-target" method="GET">
					<input type="hidden" id="active-mapping"/>
					<input type="hidden" name="ref" id="referenceFormulation"/>
					<input type="hidden" name="src" id="source_uri"/>
					<h4>Iterator select</h4>
					<div class="input-field">
						<input type="text" name="iterator" id="preview-iterator" value=""/>
						<label for="preview-iterator" class="active">Iterator</label>
					</div>
					</form>
					<div id="preview-content row input-field">
						<iframe name="preview-target" id="preview-target" frameborder="0" border="0" height="200" class="col s12"></iframe>
					</div>
					
				</div>
				<div class="modal-footer">
					      <a href="#!" class="modal-action waves-effect waves-green btn-flat " onclick="do_preview()">Preview</a>
					      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat " onclick="update_iterator()">OK</a>
				</div>
			</div>
			
			<div id="output-panel">
				<div class="col s6">
					<div class="card">
						<div class="card-content">
						<span class="card-title">RML Output</span>
						
						<a onclick="run_rml()" class="btn btn-sm btn-primary green tooltipped right" data-position="bottom" data-delay="10" data-tooltip="Run RML"><i class="material-icons">play_arrow</i></a>
						<a style="margin-right:0.6em" onclick="rdf_viz_src()" class="btn btn-floating right btn-default orange darken-1 tooltipped" data-position="left" data-delay="10" data-tooltip="Visualize RML as RDF Graph">Viz</a>

						<div class="col s12 input-field">
							<textarea name="rml-out" id="rml-out" cols="30" rows="10" class="materialize-textarea"></textarea><br />
						</div>
						<div class="col s12" id="map_vars">
							<div class="col s5 input-field">
								<input type="text" id="mapvar-{{id}}" />
								<label for="mapvar-{{id}}">Variable name</label>
								</div>
							<div class="col s5 input-field">
								<input type="text" id="mapval-{{id}}" />
								<label for="mapval-{{id}}">Value</label>
								</div>
							<div class="col s2" style="padding-top:1em;">
								<a href="#!" class="btn-floating btn-small red" onclick="remove_var(this)"><i class="material-icons">delete</i></a>
								</div>
						</div>
						<div class="row">
							<div class="col s12">
								<a class="btn btn-sm right tooltipped" data-tooltip="Assign value(s) to RML mapping variable(s)" onclick="add_var()"><i class="material-icons"></i>Add Parameter</a>
								</div>
							</div>
					</div>
					<div class="card-action">
						<a onclick="save_rml()" href="#">Save RML</a>
					</div>
					</div>
				</div>
				<div class="col s6">
					<div class="card">
						<div class="card-content">
							<span class="card-title">RDF Output</span>
							<a onclick="rdf_viz_out()" class="btn btn-floating right orange darken-1">Viz</a>
							<div class="row">
								<div class="col s12 input-field">
									<textarea name="rdf-out" id="rdf-out" cols="30" rows="10" class="materialize-textarea"></textarea><br />
								</div>
							</div>
						</div>
						<div class="card-action">
							<a href="#" onclick="save_rdf()" class="">Save RDF as</a>
						</div>
					</div>
				</div>
			</div>
			
			<div id="help-panel" class="modal modal-fixed-footer">
				<div class="modal-content">
					<div class="col s6">
						<h4>About</h4>
						<p>
						This is a wizard web application for creating extended <a href="http://rml.io">RML</a> mapping file. The mapping will hopefully exportable as Data source widget in <a href="http://linkedwidgets.org">Linked Widgets Platform</a> for instant data mashup.
						<br />
						for further information please see also:
						<ul>
							<li><a href="http://ldlabs.ifs.tuwien.ac.at">linkeddatalab</a></li>
							<li><a href="http://twitter.com/linkeddatalab">@linkeddatalab</a></li>
						</ul>
						</p>
					</div>
					<div class="col s6">
						<h4>Issues</h4>
						<ul class="collapsible" data-collapsible="accordion" id="about-issues">
							<li>
								<div class="collapsible-header">only one join condition is supported</div>
								<div class="collapsible-body"><p>Simpler are better.</p></div>
								</li>
							<li>
								<div class="collapsible-header">Predicate Map is not supported</div>
							 	<div class="collapsible-body"><p>Predicate Mapping is not supported in the editor but supported in the processor (edit RDF output manually)</p></div>
							 	</li>
							<li>
								<div class="collapsible-header">Named Graph is not supported</div>
								<div class="collapsible-body"><p><code>rr:graph</code> and <code>rr:graphMap</code> is not yet supported in the RML processor. In the meantime, the output are RDF triples in .ttl format</p></div>
								</li>
							<li>
								<div class="collapsible-header">Data Retrieval options is not (yet) supported</div>
								<div class="collapsible-body"><p>Prepare for Widget Input Terminal.</p></div>
								</li>
						</ul>
					</div>
				</div>
				<div class="modal-footer">
					<a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">OK</a>
				</div>
			</div>

			<div id="load-dialog" class="modal modal-fixed-footer">
				
				<div class="modal-content">
					<h4>Load RMLx Project</h4>
					<p>Load RML from a location</p>
					<div class="input-field">
						<input type="text" id="load-from-url" name="load-from-url" placeholder="http://">
						<label for="load-from-url">Open URL</label>
					</div>
					<p>or upload one from local filesystem</p>
					<iframe id="upload-target" name="upload-target" height="0" width="0" frameborder="0" scrolling="yes"></iframe>
					<div class="file-field input-field">
						<form id="rml-upload-form" action="upload-rml" method="post" enctype="multipart/form-data" target="upload-target">
						<div class="btn">
							<span>Select File</span>
							<input type="file" name="load-from-file" id="load-from-file">
						</div>
						<div class="file-path-wrapper">
					    	<input class="file-path validate" type="text" placeholder="Upload one or more files">
					    </div>
					    </form>
					</div>
				</div>
				<div class="modal-footer">
					<a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Close</a>
					<a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat " onclick="load_rml_dialog_handler()">OK</a>
				</div>
			</div>

			</div>
		</div>

	</body>
	<!--<script src="//code.jquery.com/jquery-2.1.3.min.js" type="text/javascript"></script>-->
	<script src="js/jquery-1.11.2.min.js" type="text/javascript"></script>
	<script src="js/materialize.min.js"></script>
	<script src="js/mustache.min.js"></script>
	<script src="js/es5-shim.min.js"></script>
	<script src="js/rdflib.js"></script>
	<script src="js/jsonld.js"></script>
	<script type="text/javascript">
		
		function parseUrl(url)
        {
            var parser = document.createElement('a');
            parser.href = url;
            return {protocol:parser.protocol, domain:parser.hostname, port:parser.port, path:parser.pathname, hash:parser.hash, server:parser.host, param:parser.search}
        }
        var myloc = parseUrl(window.location.href)
        var rml_changed = false

		var ctnum=0
		var potpl=$(".pomap").html()
		$(".pomap").html("")

		var paramtpl = $(".params-list").html()
		var optpl = $("#operator").html()
		$(".xform").html('')
		
		var disableAnimation=false

		//var tpl=$("#mappingbody li").html()
		var tpl=$("#mappingbody .tripleMapping").html()
		$("#mappingbody").html("")
		
		var vartpl = $("#map_vars").html()
		$("#map_vars").html('')

		function isCURIE(uri)
		{
			var scheme = uri.substr(0, uri.indexOf(':'))
			return (uri.indexOf(':')>0 && uri.indexOf('/')==-1 && (scheme != 'http' || scheme != 'https' || scheme != 'urn'))
		}

		function getLocalName(uri)
		{
			//if compact URI
			var result=""
			if(isCURIE(uri))
			{
				result = uri
			}
			else{
				p = parseUrl(uri)
				if(p.hash.length>0)
				{
					result=p.hash.substr(1)
					if(p.param.length>0)
						result += '?'+p.param
				}
				else {
					result = p.path.substr(p.path.lastIndexOf('/')+1)
				}
			}	
			return result

		}
		
		function add_pomap(e)
		{
			rowdiv = document.createElement('div')
			$(rowdiv).addClass("card col s12 pomapitem blue lighten-4")
			
			ctnum += 1
			rowdiv.id=Mustache.render('pomapitem-{{id}}', {id:ctnum})
			$(rowdiv).html(Mustache.render(potpl, {id:ctnum}))
			
			//in case click on the span instead the button element
			var startEl = e			
			$('#'+startEl.parentElement.parentElement.id+' .pomap').append(rowdiv)
			$('select').material_select();
			
			if(!disableAnimation)
 			$('html, body').animate({
          		scrollTop: $(rowdiv).offset().top
        	}, 2000);
			return {el:rowdiv, id:ctnum}
		}

		function add_xform(e, fn)
		{
			rowdiv = document.createElement('div')
			$(rowdiv).addClass("col s12 operator card green lighten-4")

			ctnum += 1
			var myid = ctnum
			rowdiv.id=Mustache.render('xform-{{id}}', {id:myid})
			$(rowdiv).html(Mustache.render(optpl, {id:myid}))
			
			//in case click on the span instead the button element
			var startEl = e
			$('#'+startEl.parentElement.parentElement.id+' .xform').append(rowdiv)
			if(fn)
			{
				$('#fn-'+myid).val(fn)
				//console.log(fn, $('#fn-'+myid).val())
			}
			update_fn($('#fn-'+myid)[0])
			$('select').material_select();
			
			if(!disableAnimation)
			$('html, body').animate({
          		scrollTop: $(rowdiv).offset().top
        	}, 2000);
			return {el:rowdiv, id:myid}
		}

		function add_var()
		{
			var div=document.createElement('div')
			ctnum += 1
			$(div).html(Mustache.render(vartpl, {id:ctnum}))
			$("#map_vars").append(div)
			return {el:div, id:ctnum}
		}

		function remove_var(e)
		{
			$(e.parentElement.parentElement).remove()
		}

		function get_map_vars()
		{
			var names = {}
			var vars = {}
			tmp = $("#map_vars input").each(function(idx,el){
				vn = el.id.substr(3,3)
				vi = el.id.substr(7)
				if(vn=="var")
					names[vi] = $(el).val()
				else if(vn=="val")
					vars[names[vi]] = $(el).val()
			})
			return vars
		}
		
		function add_mapping()
		{
			ctnum += 1
			var divid = ctnum
			var card=document.createElement('div')
			$(card).addClass("card tripleMapping col s4")
			$(card).attr("id", Mustache.render("mapping-{{id}}", {id:ctnum}))
			
			$(card).html(Mustache.render(tpl, {id:ctnum}))
			
			$("#mappingbody").append(card)
			$('select').material_select();
			$('#collapsible-'+divid).collapsible({accordion:true})
			return {el:card, mapid:divid}
		}
		
		//should have additional dialog (to also choose import source: local or web)
		
		function add_mapping_csv()
		{
			var mymap = add_mapping()
			$('#mapQL-'+mymap.mapid).val(QL('CSV').uri)
			$('#maptitle-'+mymap.mapid).val(Mustache.render("#UntitledCSV{{mapid}}", mymap))
			//$('#source-'+mymap.mapid).text('CSV')
			update_ql(mymap.el)
		}
		
		function add_mapping_xml()
		{
			var mymap = add_mapping()
			$('#mapQL-'+mymap.mapid).val(QL('XPath').uri)
			$('#maptitle-'+mymap.mapid).val(Mustache.render("#UntitledXML{{mapid}}", mymap))
			//$('#source-'+mymap.mapid).text('XML ')
			update_ql(mymap.el)
		}
		
		function add_mapping_json()
		{
			var mymap = add_mapping()
			$('#mapQL-'+mymap.mapid).val(QL('JSONPath').uri)
			$('#maptitle-'+mymap.mapid).val(Mustache.render("#UntitledJSON{{mapid}}", mymap))
			//$('#source-'+mymap.mapid).text('JSON ')
			update_ql(mymap.el)
		}
		
		function remove_po(e)	{ $(e.parentElement.parentElement).remove() }
		function update_omap(e)
		{
			//var pid=e.parentElement.parentElement.id
			var pid=e.id.substr(5)
			//console.log(e.id, pid)
			switch($(e).val())
			{
				case 'constant':
				case 'reference':
					show_literal_options(pid)
					$('#pomap-body-'+pid+' .joinval').hide()
					$('#pomap-body-'+pid+' .cref label').text("Value")
				break;
				case 'template':
					show_literal_options(pid)
					$('#pomap-body-'+pid+' .joinval').hide()
					$('#pomap-body-'+pid+' .cref label').text("Value")
				break;
				case 'join':
					hide_literal_options(pid)
					$('#pomap-body-'+pid+' .cref label').text("Parent Mapping")
					$('#pomap-body-'+pid+' .joinval').show()
				break;
			}
		}

		function add_param(xid, pname)
		{
			ctnum += 1
			var divid = ctnum
			var param=document.createElement('div')

			$(param).addClass("paramMapping input-field")
			$(param).attr("id", Mustache.render("mapping-{{id}}", {id:divid}))
			$(param).attr("pname", Mustache.render("mapping-{{id}}", {id:divid}))

			$(param).html(Mustache.render(paramtpl, {id:pname, ctr:xid}))
			
			return param
		}

		function extract_param(fromuri)
		{
			fndef = fromuri.replace('}','').split("{")
			if(fndef.length>1)
			{
				return fndef[1]
			}
			else return ''
		}

		function update_fn_http(e)
		{
			var pid=e.parentElement.parentElement.parentElement.id
			console.log(e.id.substr(14), pid, pid.substr(11))		
			var pctnum = e.id.substr(14)
			//console.log($('#fn-httpuri-'+pctnum).val())
			fndef = $('#fn-httpuri-'+pctnum).val().replace('}','').split("{")
			//console.log(fndef)
			if(fndef.length>1)
				fnparam = fndef[1].split(',')
			else
				fnparam = []
			num_param = fnparam.length
			//console.log($('#xform-'+ctnum+' .params-list'))
			$('#xform-'+pctnum+' .params-list').html('')
			for(i=0; i<num_param; i++)
			{
				//console.log(fnparam[i])
				var pctl = add_param(pctnum, fnparam[i])
				//console.log(pctl)
				//console.log($('#xform-'+pctnum+' .params-list'))
				$('#xform-'+pctnum+' .params-list').append(pctl)
			}
			$('select').material_select();
		}

		function update_fn(e)
		{
			var pid=e.id.substr(3)
			$('#xform-title-'+pid).text($('#'+e.id+' option:selected').text())
			fndef = $(e).val()
			if(fndef=="http")
			{
				$('#fn-httpopt-'+pid).show()
				fnparam = []
			}
			else 
			{
				$('#fn-httpopt-'+pid).hide()	
				fnparam = $(e).find("option:selected").attr('data-param')
				fnparam = (fnparam!=undefined)?fnparam.split(','):[]
			}
			num_param = fnparam.length
			$('#xform-'+pid+' .params-list').html('')
			for(i=0; i<num_param; i++)
			{
				console.log(fnparam[i])
				var pctl = add_param(pid, fnparam[i])
				$('#xform-'+pid+' .params-list').append(pctl)
			}
			$('select').material_select();
		}
		
		function load_rml()
		{
			$('#load-from-url').val(myloc.protocol+"//"+myloc.server+"/rmlx/rml-source/example3")
			$('#load-dialog').openModal()
			/*
			rmlUrl = prompt("Please enter the location of RML", myloc.protocol+"//"+myloc.server+"/mashup/rml-source/example3")
			if(rmlUrl != null)
			{
				loadRML(rmlUrl)
			}*/
		}

		function load_rml_dialog_handler()
		{
			var rml_url = $('#load-from-url').val()
			var rml_file = $('#load-from-file').val()
			console.log(rml_url, rml_file)
			if(rml_file != '')
			{
				if(rml_file.substr(rml_file.lastIndexOf('.')+1) != 'ttl')
				{
					Materialize.toast('incorrect extension, should have been .ttl', 5000)
				}
				else
				{
					$('#rml-upload-form')[0].submit()
				}
			}
			else
			{
				loadRML(rml_url)
			}
		}

		function success_upload(token)
		{
			loadRML(myloc.protocol+"//"+myloc.server+"/rmlx/rml-source/"+token)
		}

		function clear_rml()
		{
			$("#mappingbody").html("")
			$("#rdf-out").val("")
		}

		function hide_literal_options(id)
		{
			$('#mapObjttypec-'+id).hide()
			$('#mapObjdtypec-'+id).hide()
			$('#mapObjdtc-'+id).hide()
			$('#mapObjlanguagec-'+id).hide()
			$('#mapObjlangc-'+id).hide()
		}

		function show_literal_options(id)
		{
			$('#mapObjttypec-'+id).show()
			$('#mapObjdtypec-'+id).show()
			$('#mapObjdtc-'+id).hide()
			$('#mapObjlanguagec-'+id).show()
			$('#mapObjlangc-'+id).hide()
		}

		function arrayUnique(array) {
			// @rdfs:seeAlso http://stackoverflow.com/a/1584377
			var a = array.concat();
			for(var i=0; i<a.length; ++i) {
			    for(var j=i+1; j<a.length; ++j) {
			        if(a[i] === a[j])
			            a.splice(j--, 1);
			    }
			}

			return a;
		}
		
		function loadRML(docURI)
		{
			disableAnimation = true
			docUrl = parseUrl(docURI)
			if(docUrl.domain != myloc.domain)
				docURI = myloc.protocol+"//"+myloc.server+"/rmlx/rml-proxy?url="+docURI;
			console.log(docURI)
			var kb = $rdf.graph()
			fetch = $rdf.fetcher(kb)
			var progress = document.createElement('div')
			progress.id = 'progress-indicator'
			$(progress).addClass('progress')
			$(progress).html(Mustache.render('<div id="progress-control" class="determinate" style="width:{{percent}}%"', {percent:0}))
			$('#main-container').prepend(progress)
			fetch.nowOrWhenFetched(docURI,docURI,function(ok, body, xhr){
				
				//console.log(kb.namespaces)
				clear_rml()
				
				$('#rml-out').val(kb.serialize(docURI, 'text/n3'))
				console.log(kb.serialize(docURI, 'text/n3'))
				var tmp =  []
				tmp = tmp.concat(kb.each(undefined, RML('logicalSource')))
				console.log(tmp)
				tmp = tmp.concat(kb.each(undefined, RR('subjectMap')))
				tmp = tmp.concat(kb.each(undefined, RMLX('transform')))
				tmp = arrayUnique(tmp)
				
				for(var m=0; m<tmp.length; ++m)
				{
					var map = tmp[m]
					var src = kb.the(map, RML('logicalSource'))
					var mymap = add_mapping()

					$('#maptitle-'+mymap.mapid).val(map.uri)
					//console.log(mymap.mapid)
					if(src==undefined)
					{
						$('#mapSrcType'+mymap.mapid).val('no-source')
						$('#mapSrcType'+mymap.mapid).trigger('change')
					}
					else
					{
						if(kb.the(src, RML('sourceName')) != undefined)
							$('#mapSource-'+mymap.mapid).val(kb.the(src, RML('sourceName')).value)
						else if(kb.the(src, RMLX('sourceTemplate')) != undefined)
						{
							$('#mapSrcType-'+mymap.mapid).val(HTTP('requestURI'))
							$('#mapSource-'+mymap.mapid).val(kb.the(src, RMLX('sourceTemplate')).value)
						}
						else
							$('#mapSource-'+mymap.mapid).val(kb.the(src, RML('source')).value)

						if(kb.the(src, RML('queryLanguage')) != undefined)
							$('#mapQL-'+mymap.mapid).val(kb.the(src, RML('queryLanguage')).value).trigger("change")
						else
							$('#mapQL-'+mymap.mapid).val(kb.the(src, RML('referenceFormulation')).value).trigger("change")
						//$('#mapQLs-'+mymap.mapid).val(kb.the(src, RML('referenceFormulation')).value).trigger("change")
						var iter = kb.the(src, RML('iterator'))
						if(iter !== undefined)
						{
							$('#mapIterator-'+mymap.mapid).val(iter.value)
							$('label[for=mapIterator-'+mymap.mapid+']').addClass("active")
						}
					}
					
					var subj = kb.the(map, RR('subjectMap'))
					//console.log(subj)
					//console.log(kb.any(subj, undefined, undefined))
					stermtype = kb.the(subj, RR('termType'))
					//console.log(stermtype)
					sbnode = false
					if(stermtype != undefined)
					{
						switch(stermtype.uri)
						{
							case RR('IRI').uri: break;
							case RR('BlankNode').uri: 
								sbnode = true
								$('#mapSubjIsBNode-'+mymap.mapid).prop('checked', true); 
								break;
						}
					}
					template = kb.the(subj, RR('template'))
					if(template == undefined)
						if(sbnode)
							template = {value:''}
						else
							template = kb.the(subj, RR('constant'))
					$('#mapSubj-'+mymap.mapid).val(template.value)
					var stmp = kb.each(subj, undefined)
					//console.log(stmp)
					for(var s=0;s<stmp.length;++s)
					{
						if(stmp[s].uri==RR('class').uri){
							$('#mapCls-'+mymap.mapid).val(kb.the(subj, stmp[s]).value)
							$('label[for=mapCls-'+mymap.mapid+']').addClass("active")
							update_map_title(mymap.el)
						}
					}
					var xforms = kb.each(map, RMLX('transform'))
					add_xform_el = $('#add-xform-'+mymap.mapid)[0]
					$.each(xforms, function(k,vobj){
						
						var vovar = kb.the(vobj, RMLX('outputVar')).value
						var vfn = kb.the(vobj, RMLX('function'))
						if(vfn instanceof $rdf.BlankNode)
						{
							httpmethod = kb.the(vfn, HTTP('methodName'))
							if(httpmethod===undefined)
								httpmethod = 'GET'
							else
								httpmethod = httpmethod.value
							httpuri = kb.the(vfn, HTTP('requestURI')).value
							vfn = 'http'
							//console.log(httpmethod, httpuri)
						}
						else
						{
							vfn = vfn.uri.replace(ROP('').uri, '')
						}
						var rdiv = add_xform(add_xform_el, vfn)
						
						$('#fn-ovar-'+rdiv.id).val(vovar)
						if(vfn=='http')
						{
							$('#fn-httpmethod-'+rdiv.id).val(httpmethod)
							$('#fn-httpuri-'+rdiv.id).val(httpuri)
							update_fn_http($('#update_fn_btn-'+rdiv.id)[0])
						}
						var params = kb.each(vobj, RMLX('parameterMapping'))
						for(p=0; p<params.length; p++)
						{
							pname = kb.the(params[p], RMLX('varName')).value
							pvid = Mustache.render('{{ctr}}-{{pname}}', {ctr:rdiv.id, pname}) 
							
							pconst = kb.the(params[p], RR('constant'))
							ptempl = kb.the(params[p], RR('template'))
							pref = kb.the(params[p], RML('reference'))
							//console.log(pconst, ptempl, pref)
							//console.log($('#pvmap-'+pvid))
							if(pconst)
							{
								$('#pvmap-'+pvid).val('constant')
								$('#paramVal-'+pvid).val(pconst.value)
							}
							else if(ptempl)
							{
								$('#pvmap-'+pvid).val('template')
								$('#paramVal-'+pvid).val(ptempl.value)
							}
							else if(pref)
							{
								$('#pvmap-'+pvid).val('reference')
								$('#paramVal-'+pvid).val(pref.value)
							}
							//console.log('#pvmap-'+pvid, $('#pvmap-'+pvid).val())
						}
						
						//console.log(vovar, vfn, rdiv)
					})

					var pomap = kb.each(map, RR('predicateObjectMap'))
					add_pomap_el = $('#add-pomap-'+mymap.mapid)[0]
					//console.log(add_pomap_el)
					for(var pid=0;pid<pomap.length;++pid)
					{
						var rdiv = add_pomap(add_pomap_el)
						$('#pomap-pred-'+rdiv.id).val(kb.the(pomap[pid], RR('predicate')).value)
						//console.log(getLocalName($('#pomap-pred-'+rdiv.id).val()))
						$('#pomap-title-'+rdiv.id).text(getLocalName($('#pomap-pred-'+rdiv.id).val()))
						var omap = kb.the(pomap[pid], RR('objectMap'))
						
						var oref = kb.the(omap, RML('reference'))
						var odtype = kb.the(omap, RR('datatype'))
						var odlang = kb.the(omap, RR('language'))
						if(odtype !== undefined)
						{
							$('#mapObjdtype-'+rdiv.id).prop('checked', true)
							$('#mapObjdtc-'+rdiv.id).show()
							$('#mapObjdt-'+rdiv.id).val(odtype.value)
						}
						if(odlang !== undefined)
						{
							$('#mapObjlang-'+rdiv.id).prop('checked', true)
							$('#mapObjlangc-'+rdiv.id).show()
							$('#mapObjlang-'+rdiv.id).val(odtype.value)
						}
						if(oref !== undefined)
						{
							//console.log(oref.value, rdiv.id)
							$('#mapObj-'+rdiv.id).val(oref.value)
							$('#omap-'+rdiv.id).val('reference')
							var tref = kb.the(omap, RR('termType'))
							if(tref !== undefined)
							{
								//console.log(tref.value)
								//console.log(tref.value==RR('IRI').value)
								$('#mapObjttype-'+rdiv.id).prop('checked',tref.value==RR('IRI').value)
							}
							continue;
						}
						var oref = kb.the(omap, RR('template'))
						if(oref !== undefined)
						{
							//console.log(oref.value, rdiv.id)
							$('#mapObj-'+rdiv.id).val(oref.value)
							$('#omap-'+rdiv.id).val('template')
							var tref = kb.the(omap, RR('termType'))
							if(tref !== undefined)
							{
								//console.log(tref.value)
								//console.log(tref.value==RR('IRI').value)
								$('#mapObjttype-'+rdiv.id).prop('checked',tref.value==RR('IRI').value)
							}
							continue;
						}
						var oref = kb.the(omap, RR('constant'))
						if(oref !== undefined)
						{
							//console.log(oref.value, rdiv.id)
							$('#mapObj-'+rdiv.id).val(oref.value)
							$('#omap-'+rdiv.id).val('constant')
							var tref = kb.the(omap, RR('termType'))
							if(tref !== undefined)
								$('#mapObjttype-'+rdiv.id).prop('checked',tref.value==RR('IRI').value)
							continue;
						}
						var ojoin = kb.the(omap, RR('joinCondition'))
						if(ojoin !== undefined)
						{
							hide_literal_options(rdiv.id)
							$('#omap-'+rdiv.id).val('join')
							$('#omap-'+rdiv.id).change()
							$('#mapObj-'+rdiv.id).val(kb.the(omap, RR('parentTriplesMap')).value)
							//jcond = kb.each(omap, RR('joinCondition') )
							//TODO: implement multiple join condition (uncomment line above)
							jcond = kb.any(omap, RR('joinCondition') )
							$('#parent-'+rdiv.id).val(kb.the(jcond, RR('parent')).value)
							$('#child-'+rdiv.id).val(kb.the(jcond, RR('child')).value)
						}

					}

					$(progress).html(Mustache.render('<div id="progress-control" class="determinate" style="width:{{percent}}%"', {percent:Math.round(100*(m/tmp.length))}))
					
				}
				
				//$('.nav-tabs a[href="#mapping-panel"]').tab('show');
				$(progress).remove()
				disableAnimation = false
			});
			
		}
		
		var remoteUrl = null;
		
		function save_lw()
		{
			generate_rml()
			$.post('save-rml', {data:$('#rml-out').val()}, function(data){
				var ret = JSON.parse(data)
				window.open('widget/rml?src='+ret.token)
			})
		}
		
		function run_rml()
		{
			$.post('exec-rml?'+$.param(get_map_vars()), {data:$('#rml-out').val()}, function(data){
				$('#rdf-out').val(data).trigger('autoresize')
				if(data.length==0)
					$('#rdf-out').val('#empty result returned')
			})
		}

		function save_remote()
		{
			$.post('save-rml', {data:$('#rml-out').val()}, function(data){
				data = JSON.parse(data)
				console.log(data.token)
				if(data.token)
					loadRML(myloc.protocol+"//"+myloc.server+"/rmlx/rml-source/"+data.token)
			})
		}
		
		function save_rdf()
		{
			var rdfout = $('#rdf-out').val()
			if(rdfout != "")
				Download.save(rdfout, 'rml-output.ttl')
		}

		function rdf_viz_out()
		{
			var rdfdata = $('#rdf-out').val()
			rdf_viz(rdfdata)
		}

		function rdf_viz_src()
		{
			var rdfdata = $('#rml-out').val()
			rdf_viz(rdfdata)
		}

		function rdf_viz(rdfdata)
		{
			$.post('save-rdf', {data:rdfdata}, function(data){
				data = JSON.parse(data)
				console.log(data.token)
				console.log(myloc.protocol+"//"+myloc.server+"/rmlx/rdf-source/"+data.token)
				if(data.token)
					window.open(myloc.protocol+"//"+myloc.server+"/rmlx/rdf-viz#"+myloc.protocol+"//"+myloc.server+"/rmlx/rdf-source/"+data.token, '_blank')
			})
		}

		function remove_vocab(e)
		{
			vid = e.id.substr(3)
			$('#vocab-'+vid).remove()
		}
		
		function import_vocab()
		{
			var prefix_template = '<tr id="vocab-{{prefix}}"><td class="vocab-prefix">{{prefix}}</td><td id="vocaburi-{{prefix}}">{{vocab}}</td><td><a id="rm-{{prefix}}" href="#!" onclick="remove_vocab(this)" class="btn-floating btn-small"><i class="material-icons">delete</i></a></td></tr>'
			var vocab = $('#addvocab').val()
			if(vocab.indexOf('=') != -1)
			{
				var pair = vocab.split('=')
				$('#vlist').append(Mustache.render(prefix_template, {prefix:pair[0], vocab:pair[1]}))
				$('#addvocab').val("")
			}
			else if(vocab.substr(0,4)=="http")
			{
				$.getJSON("http://prefix.cc/reverse?format=json&uri="+vocab, function(data){
					$('#addvocab').val("")
					var prefix = Object.keys(data)[0]
					$('#vlist').append(Mustache.render(prefix_template, {prefix:Object.keys(data)[0], vocab:vocab}))
					//$("#vlist").append('<tr><td>'+prefix+'</td><td>'+vocab+'</td><td><a href="#!" onclick="remove_vocab(this)" class="btn-floating btn-small"><i class="material-icons">delete</i></a></td></tr>')
				})
			}
			else
			{
				$.getJSON("http://prefix.cc/"+vocab+".file.json", function(data){
					console.log(data)
					$('#addvocab').val("")
					var uri = data[vocab]
					//$("#vlist").append('<tr><td>'+vocab+'</td><td>'+uri+'</td></tr>')
					$('#vlist').append(Mustache.render(prefix_template, {prefix:vocab, vocab:uri}))
				})
			}
		}

		$('ul.tabs').on('click', 'a', function(e) {
		    var target = $(e.target).attr("href");
		    if(target=="#output-panel")
			  {
				generate_rml()
			  }
			  if(target=="#mapping-panel" && rml_changed)
			  {
			  	save_remote()
			  }
		});
		
		var RML = $rdf.Namespace('http://semweb.mmlab.be/ns/rml#')
		var RR = $rdf.Namespace('http://www.w3.org/ns/r2rml#')
		var QL = $rdf.Namespace('http://semweb.mmlab.be/ns/ql#')
		var XSD = $rdf.Namespace('http://www.w3.org/2001/XMLSchema#')
		var RMLX = $rdf.Namespace('http://pebbie.org/ns/rmlx#')
		var ROP = $rdf.Namespace('http://pebbie.org/ns/rmlx-functions#')
		var HTTP = $rdf.Namespace('http://www.w3.org/2011/http#')
		
		function generate_rml()
		{
			var kb = $rdf.graph()
			kb.setPrefixForURI('xsd', 'http://www.w3.org/2001/XMLSchema#')
			kb.setPrefixForURI('rml', 'http://semweb.mmlab.be/ns/rml#')
			kb.setPrefixForURI('rr', 'http://www.w3.org/ns/r2rml#')
			//console.log(kb.namespaces)
			$('.tripleMapping').each(function(idx,el){
					var mapid = el.id.substr(8)

					var title = $('#maptitle-'+mapid).val()
					if(title.substr(0,4)!='http')
						title = encodeURI(title)
					
					//create new mapping URI
					mapping_node = kb.sym( title )
					
					//kb.add(lsrc, RML('sourceName'), kb.literal($('#mapSource-'+mapid).val()))
					var map_srctype = $('#mapSrcType-'+mapid).val()
					var is_no_source = map_srctype=='no-source'
					var map_src = $('#mapSource-'+mapid).val()

					if(is_no_source)
					{
						//do not need to insert rml:logicalSource
					}
					else
					{
						lsrc = kb.bnode()
						kb.add(mapping_node, RML('logicalSource'), lsrc)

						var lp = map_src.indexOf('{'), rp = map_src.indexOf('}')
						if(lp != -1 && rp != 1 && lp < rp)
							kb.add(lsrc, RMLX('sourceTemplate'), kb.literal($('#mapSource-'+mapid).val()))
						else
							kb.add(lsrc, RML('source'), kb.literal(map_src))
						
						if($('#mapIterator-'+mapid).val() != "")
							kb.add(lsrc, RML('iterator'), kb.literal($('#mapIterator-'+mapid).val()))

						kb.add(lsrc, RML('referenceFormulation'), kb.sym($('#mapQL-'+mapid).val()))
						//kb.add(lsrc, RML('queryLanguage'), kb.sym($('#mapQL-'+mapid).val()))
					}
					
					//add subjectMap
					var smap = kb.bnode()
					kb.add(mapping_node, RR('subjectMap'), smap)
					if($('#mapSubjIsBNode-'+mapid).is(':checked'))
						kb.add(smap, RR('termType'), RR('BlankNode'))
					else
						kb.add(smap, RR('template'), kb.literal($('#mapSubj-'+mapid).val()))

					//add rr:class inside subjectMap
					var scls = $('#mapCls-'+mapid).val()
					if(scls!='')
						kb.add(smap, RR('class'), kb.sym(scls))
					//console.log($(el).find('.pomap > .pomapitem'))	

					//generate transforms
					$(el).find('.xform > .operator').each(function(id, e){
						console.log(e)
						var eid = e.id.substr(6)
						var xformnode = kb.bnode()
						kb.add(mapping_node, RMLX('transform'), xformnode)
						kb.add(xformnode, RMLX('outputVar'), $('#fn-ovar-'+eid).val())
						var fn = $('#fn-'+eid).val()
						if(fn != 'http')
						{
							kb.add(xformnode, RMLX('function'), ROP(fn))
							params = $('#fn-'+eid).find('option:selected').attr('data-param')
						}
						else
						{
							httpnode = kb.bnode()
							kb.add(xformnode, RMLX('function'), httpnode)
							kb.add(httpnode, HTTP('methodName'), $('#fn-httpmethod-'+eid).val())
							var httpuri = $('#fn-httpuri-'+eid).val()
							kb.add(httpnode, HTTP('requestURI'), httpuri)
							params = extract_param(httpuri)
						}
						console.log(fn, params)
						fparam = params.split(',')
						$.each(fparam, function(k,pname){
							var pmapnode = kb.bnode()
							kb.add(xformnode, RMLX('parameterMapping'), pmapnode)
							kb.add(pmapnode, RMLX('varName'), pname)
							switch($('#pvmap-'+eid+'-'+pname).val())
							{
								case 'constant':
									kb.add(pmapnode, RR('constant'), $('#paramVal-'+eid+'-'+pname).val())
									break;

								case 'template':
									kb.add(pmapnode, RR('template'), $('#paramVal-'+eid+'-'+pname).val())
									break;

								case 'reference':
									kb.add(pmapnode, RML('reference'), $('#paramVal-'+eid+'-'+pname).val())
									break;
							}
						})

					})
					
					//generate predicateObjectMaps
					$(el).find('.pomap > .pomapitem').each(function(id, e){
						var eid = e.id.substr(10)
						switch($('#omap-'+eid).val())
						{
							case 'constant':
								//kb.add(smap, kb.sym($('#pomap-pred-'+eid).val()), kb.literal($('#mapObj-'+eid).val()))
								var pomnode = kb.bnode()
								kb.add(mapping_node, RR('predicateObjectMap'), pomnode)
								kb.add(pomnode, RR('predicate'), kb.sym($('#pomap-pred-'+eid).val()))
								var omnode = kb.bnode()
								kb.add(pomnode, RR('objectMap'), omnode)
								kb.add(omnode, RR('constant'), kb.literal($('#mapObj-'+eid).val()))
								if($('#mapObjttype-'+eid).is(':checked'))
									kb.add(omnode, RR('termType'), RR('IRI'))
								if($('#mapObjdtype-'+eid).is(':checked'))
								{
									kb.add(omnode, RR('datatype'), kb.sym($('#mapObjdt-'+eid).val()))
								}
								break;
							case 'reference':
								var pomnode = kb.bnode()
								kb.add(mapping_node, RR('predicateObjectMap'), pomnode)
								kb.add(pomnode, RR('predicate'), kb.sym($('#pomap-pred-'+eid).val()))
								var omnode = kb.bnode()
								kb.add(pomnode, RR('objectMap'), omnode)
								kb.add(omnode, RML('reference'), kb.literal($('#mapObj-'+eid).val()))
								if($('#mapObjttype-'+eid).is(':checked'))
									kb.add(omnode, RR('termType'), RR('IRI'))
								if($('#mapObjdtype-'+eid).is(':checked'))
								{
									kb.add(omnode, RR('datatype'), kb.sym($('#mapObjdt-'+eid).val()))
								}
								break;
							case 'template':
								var pomnode = kb.bnode()
								kb.add(mapping_node, RR('predicateObjectMap'), pomnode)
								kb.add(pomnode, RR('predicate'), kb.sym($('#pomap-pred-'+eid).val()))
								var omnode = kb.bnode()
								kb.add(pomnode, RR('objectMap'), omnode)
								kb.add(omnode, RR('template'), kb.literal($('#mapObj-'+eid).val()))
								if($('#mapObjttype-'+eid).is(':checked'))
									kb.add(omnode, RR('termType'), RR('IRI'))
								if($('#mapObjdtype-'+eid).is(':checked'))
								{
									kb.add(omnode, RR('datatype'), kb.sym($('#mapObjdt-'+eid).val()))
								}
								break;
							case 'join':
								var pomnode = kb.bnode()
								kb.add(mapping_node, RR('predicateObjectMap'), pomnode)
								kb.add(pomnode, RR('predicate'), kb.sym($('#pomap-pred-'+eid).val()))
								var omnode = kb.bnode()
								kb.add(pomnode, RR('objectMap'), omnode)
								kb.add(omnode, RR('parentTriplesMap'), kb.sym($('#mapObj-'+eid).val()))
								var jnode = kb.bnode()
								kb.add(omnode, RR('joinCondition'), jnode)
								kb.add(jnode, RR('parent'), kb.literal($('#parent-'+eid).val()))
								kb.add(jnode, RR('child'), kb.literal($('#child-'+eid).val()))
								break;
						}
					})
					
			})
			$('#rml-out').val(kb.serialize('', 'text/turtle')).trigger('autoresize')
		}

		//http://stackoverflow.com/questions/12718210/how-to-save-file-from-textarea-in-javascript-with-a-name
		var Download = {
			click : function(node) {
				var ev = document.createEvent("MouseEvents");
				ev.initMouseEvent("click", true, false, self, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
				return node.dispatchEvent(ev);
			},
			encode : function(data) {
					return 'data:application/octet-stream;base64,' + btoa( data );
			},
			link : function(data, name){
				var a = document.createElement('a');
				a.download = name || self.location.pathname.slice(self.location.pathname.lastIndexOf('/')+1);
				a.href = data || self.location.href;
				return a;
			}
		};
		
		Download.save = function(data, name){
			this.click(
				this.link(
					this.encode( data ),
					name
				)
			);
		};
		
		function save_rml()
		{
			Download.save($('#rml-out').val(), 'mapping.rml.ttl')
		}

		function download_rml()
		{
			generate_rml()
			save_rml()
		}

		$('#rml-out').bind('input propertychange', function() {
			rml_changed = true;
			console.log("changed")
		});

		function select_iterator(e)
		{
			console.log(e)
			mapid=e.id.substr(e.id.lastIndexOf('-')+1)
			$('#active-mapping').val(mapid)
			$('#referenceFormulation').val($('#mapQL-'+mapid).val())
			$('#source_uri').val($('#mapSource-'+mapid).val())
			$('#preview-iterator').val($('#mapIterator-'+mapid).val())
			$('#preview-modal').openModal()
			
			//should display a modal window which then make a preview of the data and guide user to select the iterator
			//depends on the referenceFormulation language, show different viewer/picker
		}

		function show_about()
		{
			$('#help-panel').openModal()
		}

		function do_preview()
		{
			mapid = $('#active-mapping').val()
			$('#preview-form')[0].submit()
		}

		function update_iterator()
		{
			mapid = $('#active-mapping').val()
			$('#mapIterator-'+mapid).val($('#preview-iterator').val())
		}

		function update_dtype(e)
		{
			var pid=e.id.substr(12)
			if($(e).prop('checked'))
				$("#mapObjdtc-"+pid).show()
			else
			{
				$("#mapObjdtc-"+pid).hide()
			}
		}

		$(document).ready(function(){
    		$('ul.tabs').tabs();
    		$('#about-issues').collapsible({accordion:true})

    		$(document).on('click.card', '.card', function (e) {
		      if ($(this).find('> .card-reveal').length) {
		        if ($(e.target).is($('.card-reveal .card-title')) || $(e.target).is($('.card-reveal .card-title i'))) {
		          // Make Reveal animate down and display none
		          $(this).find('.card-reveal').velocity(
		            {translateY: 0}, {
		              duration: 225,
		              queue: false,
		              easing: 'easeInOutQuad',
		              complete: function() { $(this).css({ display: 'none'}); }
		            }
		          );
		            $(this).velocity({height:$(this).data('height')},{duration:225});
		        }
		        else if ($(e.target).is($('.card .activator')) ||
		                 $(e.target).is($('.card .activator i')) ) {
		          $(e.target).closest('.card').css('overflow', 'hidden');
		          $(this).data('height',$(this).css('height')+40).find('.card-reveal').css({ display: 'block',height:'auto'}).velocity("stop", false).velocity({translateY: '-100%'}, {duration: 300, queue: false, easing: 'easeInOutQuad'});
		              $(this).velocity({height:$(this).find('.card-reveal').height()+40},{duration:300});
		        }
		      }
		      $('.card-reveal').closest('.card').css('overflow', 'hidden');
		    });

  		});

  		function update_card_title(e)
  		{
  			pid=e.id.substr(11)
  			$('#pomap-title-'+pid).text(getLocalName($('#pomap-pred-'+pid).val()))
  		}

  		function update_language(e)
  		{
  			//Materialize.toast('NotImplemented: '+e.id, 1000)
  			//console.log(e.id)
  			var pid=e.id.substr(15)
			if($(e).prop('checked'))
				$("#mapObjlangc-"+pid).show()
			else
			{
				$("#mapObjlangc-"+pid).hide()
			}
  		}

  		function update_subjtempl(e)
  		{
  			mapid=e.id.substr(15)
  			$('#mapSubj-'+mapid).prop('disabled', $(e).prop('checked'))
  		}

  		function update_map_title(e)
  		{
  			if(e !== undefined) {
  				mapid=e.id.substr(e.id.lastIndexOf('-')+1)
  				$('#mapping-title-'+mapid).text(getLocalName($('#mapCls-'+mapid).val()))
  			}
  		}

  		function update_sourcetype(e)
  		{
  			mapid=e.id.substr(e.id.lastIndexOf('-')+1)
  			var is_no_source=($(e).val()=="no-source")
  			if(is_no_source)
  			{
  				$('#source-'+mapid).text('Empty')
  				$('#seliterator-'+mapid).addClass('disabled')
  			}
  			else
  			{
  				$('#seliterator-'+mapid).removeClass('disabled')
  				update_ql(e)
  			}
  			$('#mapSource-'+mapid).prop('disabled', is_no_source)
  			$('#mapQL-'+mapid).prop('disabled', is_no_source)
  			$('#mapIterator-'+mapid).prop('disabled', is_no_source)
  			$('#seliterator-'+mapid).prop('disabled', is_no_source)
  			$('#mapQL-'+mapid).material_select()
  		}

  		function update_ql(e)
  		{
  			mapid=e.id.substr(e.id.lastIndexOf('-')+1)
  			$('#source-'+mapid).text($('#mapQL-'+mapid+' option:selected').attr("data-content"))
  			$('#mapQL-'+mapid).material_select()
  		}

  		var old_ival = '';
  		function test_autocomplete(e)
  		{
  			var termtype = $(e).attr('data-termtype')
  			var ival = $(e).val()
  			if(isCURIE(ival)){
  				var lval = ival.substr(0,ival.indexOf(':'))
  				//console.log(ival, lval, $('#vocab-'+lval).length)
  				if($('#vocab-'+lval).length>0)
  				{
  					$(e).val(ival.replace(lval+':', $('#vocaburi-'+lval).text()))
  				}
  				else if (ival != old_ival){
	  				$.getJSON('http://lov.okfn.org/dataset/lov/api/v2/term/autocomplete?q='+ival+'&type='+termtype, function(data){
	  					$('.autocomplete-content').remove()	
	  					old_ival = ival;
	  					var data_out = []
	  					$.each(data.results, function(i,term){
	  						data_out.push({value:term.uri[0], localName:term.localName[0]})
	  					})
	  					$(e).data('array', data_out)
	  					openAutocomplete(e)
	  				})
	  			}
	  			else {
	  				openAutocomplete(e)
	  			}
  			}
  			else {

  				//console.log(old_ival, ival)
  			}
  		}

  		//--------------------------------- autocomplete begin

  		function init_autocomplete(el)
  		{
  			mapid = el.id.substr(el.id.lastIndexOf('-')+1)
  			var $array = $(el).data('array'), 
            $inputDiv = $(el).closest('.input-field'); // Div to append on
            if ($array !== undefined && $array !== '') {
            	//console.log('create autocomplete '+mapid, $inputDiv)
  			var $html = '<ul class="autocomplete-content " id="autocomplete-'+mapid+'">';//hide

            for (var i = 0; i < $array.length; i++) {
              // If path and class aren't empty add image to auto complete else create normal element
              if ($array[i]['path'] !== '' && $array[i]['path'] !== undefined && $array[i]['path'] !== null && $array[i]['class'] !== undefined && $array[i]['class'] !== '') {
                $html += '<li class="autocomplete-option"><img src="' + $array[i]['path'] + '" class="' + $array[i]['class'] + '"><span>' + $array[i]['value'] + '</span></li>';
              } 
              else if($array[i]['localName'] !== '' && $array[i]['localName'] !== undefined && $array[i]['localName'] !== null) {
              	$html += '<li class="autocomplete-option"><div style="color:white;">' + $array[i]['localName'] + '</div><span style="font-size:0.8rem;">'+$array[i]['value']+'</span></li>';
              }
              else {
                $html += '<li class="autocomplete-option"><span>' + $array[i]['value'] + '</span></li>';
              }
            }

            $html += '</ul>';

            $inputDiv.append($html);
            //console.log($('#autocomplete-'+mapid))
        	}
  		}

  		    function openAutocomplete(sel) {

      var input_selector = sel||'input[type=text], input[type=password], input[type=email], input[type=url], input[type=tel], input[type=number], input[type=search], textarea';


      /**************************
       * Auto complete plugin  *
       *************************/

      $(input_selector).each(function() {
        var $input = $(this);
        mapid = this.id.substr(this.id.lastIndexOf('-')+1)

        if ($input.hasClass('autocomplete')) {

          if($input){
          	if($('#autocomplete-'+mapid).length == 0)
          	{
        		init_autocomplete(this)
          	}
          	else
          	{
          		$('#autocomplete-'+mapid+' .autocomplete-option').removeClass('hide')
          	}
          // Check if "data-array" isn't empty
          
            // Create html element
            
             // Set ul in body
            // End create html element

            function highlight(string) {
              $('.autocomplete-content li').each(function() {
                var matchStart = $(this).text().toLowerCase().indexOf("" + string.toLowerCase() + ""),
                  matchEnd = matchStart + string.length - 1,
                  beforeMatch = $(this).text().slice(0, matchStart),
                  matchText = $(this).text().slice(matchStart, matchEnd + 1),
                  afterMatch = $(this).text().slice(matchEnd + 1);
                $(this).html("<span>" + beforeMatch + "<span class='highlight'>" + matchText + "</span>" + afterMatch + "</span>");
              });
            }

            // Perform search
            $(document).on('keyup', $input, function() {
              var $val = $input.val().trim(),
                $select = $('.autocomplete-content');
                if(isCURIE($val)) $lval = $val.substr($val.indexOf(':')+1)
              // Check if the input isn't empty
              $select.css('width', $input.width());

              if ($val != '') {
                $select.children('li').addClass('hide');
                $select.children('li').filter(function() {
                  $select.removeClass('hide'); // Show results

                  // If text needs to highlighted
                  if ($input.hasClass('highlight-matching')) {
                    highlight($val);
                  }
                  var check = true;
                  for (var i in $lval) {
                    if (typeof($lval[i])=='string' && ($lval[i].toLowerCase() !== $(this).text().toLowerCase()[i]))
                      check = false;
                  };
                  //console.log(check, $lval, $val, $(this).find('div').text(), $(this).find('div').text().toLowerCase().indexOf($lval.toLowerCase()) !== -1)
                  return check ? $(this).find('div').text().toLowerCase().indexOf($lval.toLowerCase()) !== -1 : false;
                }).removeClass('hide');
              } 
              else {
              	//console.log('hide children')
                $select.children('li').addClass('hide');
              }
            });

            // Set input value
            //$('.autocomplete-option').click(function() {
            $('#autocomplete-'+mapid+' .autocomplete-option').click(function() {
            //console.log(this, $('#autocomplete-'+mapid))
              $input.val($(this).find('span').text().trim());
              //$('.autocomplete-option').addClass('hide');
              $('#autocomplete-'+mapid).addClass('hide')
              $('#autocomplete-'+mapid).remove()
              $input.trigger('change')
              //$('.autocomplete-content').remove()
            });
          } 
          else {
            return false;
          }
        }
      });



    }


  		//--------------------------------- autocomplete end
	</script>
</html>