<?php

if(isset($_GET['uri'])){
	$str = $_GET['uri'];
	$pos = strrpos($str, '.')+1;
	$ext = substr($str, $pos, strlen($str)-$pos);
	//echo $ext;
	switch ($ext){
		case 'n3':
			header('Content-type: text/rdf+n3;');
			break;
		case 'ttl':
			header('Content-type: text/turtle;');
			break;
	}
	echo file_get_contents($str);
}