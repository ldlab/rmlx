<?php

$route = array();
$routevars = array();
foreach( glob('ext/*.php') as $module)
    require_once($module);

//bootstrapping code
if (file_exists('boot.php')) require_once('boot.php');

foreach( glob('mod/*.php') as $module)
    require_once($module);


function escape($str){
    return str_replace('/', '\/',$str);
}
function route($path,$handler,$method='GET'){
    global $route, $routevars;
    $epath = escape($path);
    $epath = preg_replace("/\/:(\w+)/s", "/<$1:name>", $epath);
    $n = preg_match_all('<(\w+):(\w+)>', $epath, $matches);
    if(intval($n) > 0){
        $mapping = array();
        foreach($matches[0] as $ms)
        {
            $mapping[] = explode(":", $ms);
        }
        
        foreach($mapping as $k=>$m){
            switch($m[1])
            {
                case 'int': $regex = '(\d+)'; break;
                case 'domain': $regex = '([\w\d]+([\-\.]{1}[\w\d]+)*\.\w{2,6})'; break;
                case 'path': $regex = '(.*)'; break;
                default: $regex = '([\d\w]+[\.\-_\w\d]+)'; break;
            }
            $epath = str_replace("<".$m[0].":".$m[1].">", $regex, $epath);
        }
        $routevars[$epath] = array();
        foreach($mapping as $k=>$m){
            $routevars[$epath][$m[0]] = $k;
        }
    }

    if(isset($route[$epath]) && is_array($route[$epath])){
        $route[$epath][$method] = $handler;
    }
    else{
        $route[$epath] = array($method=>$handler);
    }
    //echo $json->encode($routevars[$epath])." __".htmlspecialchars($epath)."<br/>";
}
function pr($str){
    return '/^'.$str.'$/s';
}

function template($template,$vars=array()){
    $tpl = new Savant3();
    $tpl->vars = $vars;
    $tpl->root = str_replace('/index.php', '', $_SERVER['SCRIPT_NAME']);
    $ftpl = 'tpl/'.$template.'.tpl';
    $tpl->display($ftpl);
}

?>